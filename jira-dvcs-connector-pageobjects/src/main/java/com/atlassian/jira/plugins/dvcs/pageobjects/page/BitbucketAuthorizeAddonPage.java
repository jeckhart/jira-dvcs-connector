package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * The Bitbucket "Authorize addon" page used to authorize requested scopes etc. for a connect addon.
 */
public class BitbucketAuthorizeAddonPage implements Page {
    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "addon-authorize")
    private PageElement addonAuthorizeDiv;

    @ElementBy(xpath = "//button[@value='approve']")
    private PageElement grantAccessButton;

    @ElementBy(xpath = "//button[@value='deny']")
    private PageElement cancelButton;

    @Override
    public String getUrl() {
        return "/site/addons/authorize";
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue("The 'Authorize Addon' page did not load", addonAuthorizeDiv.timed().isPresent());
    }

    public void grantAccess() {
        waitUntilTrue("The 'Grant Access' button is not visible.", grantAccessButton.timed().isVisible());
        grantAccessButton.click();
    }

    public void denyAccess() {
        waitUntilTrue("The 'Cancel' button is not visible.", cancelButton.timed().isVisible());
        cancelButton.click();
    }
}
