package com.atlassian.jira.plugins.dvcs.pageobjects.util;

import com.atlassian.pageobjects.elements.PageElement;

/**
 * Utility methods for things related to Atlassian Connect Integration.
 */
public class AciPageObjectUtils {
    private static final String OPTION_TEXT = "Bitbucket Cloud";
    private static final String OPTION_VALUE = "bitbucket";

    /**
     * Choose a button for adding an org using the old flow (i.e. specifying an Oauth key and secret).
     * <p>
     * If the ACI dark feature is on, we need to select the {@code linkGithubAccountButton}, and then manually
     * add in Bitbucket as an option if we want to add a Bitbucket org using the old flow
     * (see {@link #addBitbucketAsDvcsType(PageElement)}).
     * <p>
     * If the ACI dark feature is off, we simply use the only add org button on the page, {@code linkRepositoryButton}.
     *
     * @param linkRepositoryButton    button to add an org when ACI feature is disabled
     * @param linkGithubAccountButton button to add a Github org when ACI feature is enabled - we use this to
     *                                add a Bitbucket org using the old flow.
     * @param aciEnabled              true if the ACI dark feature is enabled in JIRA
     * @return the button to use for adding an org using the old pre-ACI flow
     */
    public static PageElement oldFlowAddOrgButton(
            PageElement linkRepositoryButton,
            PageElement linkGithubAccountButton,
            boolean aciEnabled) {
        return aciEnabled ? linkGithubAccountButton : linkRepositoryButton;
    }

    /**
     * When the ACI dark feature is enabled we need to use the old dialog if we want to add a Bitbucket organisation
     * using the old flow, by specifying an oauth key and secret. We do this by clicking the add Github organisation
     * button on the new DVCS admin UI, and then manually adding bitbucket back in as an option, so we can select it.
     * <p>
     * At this point we assume the add Github org button has been clicked, and here we add Bitbucket as an option
     * in the dvcs type select element.
     *
     * @param dvcsTypeSelect the select element for choosing a dvcs type - we will add Bitbucket as an option
     */
    public static void addBitbucketAsDvcsType(PageElement dvcsTypeSelect) {
        dvcsTypeSelect.javascript().execute("var option = document.createElement(\"option\");\n"
                + "option.text = \"" + OPTION_TEXT + "\";\n"
                + "option.value = \"" + OPTION_VALUE + "\";\n"
                + "arguments[0].insertBefore(option, arguments[0].firstChild);");
    }
}
