package com.atlassian.jira.plugins.dvcs.spi.bitbucket;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.crypto.Encryptor;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.BasicAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialVisitor;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.UnauthenticatedCredential;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.AuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.ACIJwtAuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.BasicAuthAuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth.NoAuthAuthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.scribe.ThreeLegged10aOauthProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.scribe.TwoLeggedOauthProvider;
import com.atlassian.jira.plugins.dvcs.util.DvcsConstants;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;

@Named
public class DefaultBitbucketClientBuilderFactory implements BitbucketClientBuilderFactory {
    private static final Logger log = LoggerFactory.getLogger(DefaultBitbucketClientBuilderFactory.class);

    private final Encryptor encryptor;
    private final HttpClientProvider httpClientProvider;

    @Nullable
    private final EventPublisher eventPublisher;

    @Nullable
    private final ServiceAccessor<ACIJwtService> jwtServiceAccessor;

    @Nullable
    private final ServiceAccessor<ACIRegistrationService> registrationServiceAccessor;

    @Inject
    public DefaultBitbucketClientBuilderFactory(
            @ComponentImport @Nonnull final PluginAccessor pluginAccessor,
            @Nonnull final Encryptor encryptor,
            @Nonnull final HttpClientProvider httpClientProvider,
            @Nullable final EventPublisher eventPublisher,
            @Nullable final ServiceAccessor<ACIJwtService> jwtServiceAccessor,
            @Nullable final ServiceAccessor<ACIRegistrationService> registrationServiceAccessor) {
        this(checkNotNull(encryptor),
                DvcsConstants.getUserAgent(checkNotNull(pluginAccessor)),
                checkNotNull(httpClientProvider),
                checkNotNull(eventPublisher),
                jwtServiceAccessor,
                registrationServiceAccessor);
    }

    public DefaultBitbucketClientBuilderFactory(
            @Nonnull final Encryptor encryptor,
            @Nonnull final String userAgent,
            @Nonnull final HttpClientProvider httpClientProvider,
            @Nullable final EventPublisher eventPublisher,
            @Nullable final ServiceAccessor<ACIJwtService> jwtServiceAccessor,
            @Nullable final ServiceAccessor<ACIRegistrationService> registrationServiceAccessor) {
        this.encryptor = checkNotNull(encryptor);
        this.httpClientProvider = checkNotNull(httpClientProvider);
        httpClientProvider.setUserAgent(userAgent);
        this.eventPublisher = eventPublisher;
        this.jwtServiceAccessor = jwtServiceAccessor;
        this.registrationServiceAccessor = registrationServiceAccessor;
    }

    @Override
    public BitbucketClientBuilder forOrganization(final Organization organization) {
        AuthProvider authProvider = createProvider(organization.getHostUrl(), organization.getName(), organization.getCredential());
        return new DefaultBitbucketRemoteClientBuilder(authProvider);
    }

    @Override
    public BitbucketClientBuilder forRepository(final Repository repository) {
        AuthProvider authProvider = createProvider(repository.getOrgHostUrl(), repository.getOrgName(), repository.getCredential());
        return new DefaultBitbucketRemoteClientBuilder(authProvider);
    }

    @Override
    public BitbucketClientBuilder noAuthClient(final String hostUrl) {
        AuthProvider authProvider = new NoAuthAuthProvider(hostUrl, httpClientProvider);
        return new DefaultBitbucketRemoteClientBuilder(authProvider);
    }

    @Override
    public BitbucketClientBuilder authClient(final String hostUrl, final String name, final Credential credential) {
        AuthProvider authProvider = createProvider(hostUrl, name, credential);
        return new DefaultBitbucketRemoteClientBuilder(authProvider);
    }

    private AuthProvider createProvider(final String hostUrl, final String name, final Credential credential) {
        return credential.accept(new AuthProviderCredentialVisitor(
                encryptor, hostUrl, name, httpClientProvider, eventPublisher, registrationServiceAccessor, jwtServiceAccessor));
    }

    private static class AuthProviderCredentialVisitor implements CredentialVisitor<AuthProvider> {
        private final Encryptor encryptor;
        private final String hostUrl;
        private final String name;
        private final HttpClientProvider httpClientProvider;
        @Nullable
        private final EventPublisher eventPublisher;
        @Nullable
        private final ServiceAccessor<ACIRegistrationService> registrationServiceAccessor;
        @Nullable
        private final ServiceAccessor<ACIJwtService> jwtServiceAccessor;

        private AuthProviderCredentialVisitor(
                final Encryptor encryptor,
                final String hostUrl,
                final String name,
                final HttpClientProvider httpClientProvider,
                final EventPublisher eventPublisher,
                final ServiceAccessor<ACIRegistrationService> registrationServiceAccessor,
                final ServiceAccessor<ACIJwtService> jwtServiceAccessor) {
            this.encryptor = encryptor;
            this.hostUrl = hostUrl;
            this.name = name;
            this.eventPublisher = eventPublisher;
            this.httpClientProvider = httpClientProvider;
            this.registrationServiceAccessor = registrationServiceAccessor;
            this.jwtServiceAccessor = jwtServiceAccessor;
        }

        @Override
        public AuthProvider visit(final BasicAuthCredential credential) {
            log.trace("Using basic authentication for '{}'", name);
            String decryptedPassword = encryptor.decrypt(credential.getPassword(), name, hostUrl);
            return new BasicAuthAuthProvider(
                    hostUrl,
                    credential.getUsername(),
                    decryptedPassword,
                    httpClientProvider);
        }

        @Override
        public AuthProvider visit(final TwoLeggedOAuthCredential credential) {
            log.trace("Using 2LO authentication for '{}'", name);
            return new TwoLeggedOauthProvider(
                    hostUrl,
                    credential.getKey(),
                    credential.getSecret(),
                    httpClientProvider);
        }

        @Override
        public AuthProvider visit(final ThreeLeggedOAuthCredential credential) {
            log.trace("Using 3LO authentication for '{}'", name);
            return new ThreeLegged10aOauthProvider(
                    hostUrl,
                    credential.getKey(),
                    credential.getSecret(),
                    credential.getAccessToken(),
                    httpClientProvider);
        }

        @Override
        public AuthProvider visit(final PrincipalIDCredential credential) {
            if (eventPublisher != null &&
                    registrationServiceAccessor != null && registrationServiceAccessor.isAvailable() &&
                    jwtServiceAccessor != null && jwtServiceAccessor.isAvailable()) {
                log.trace("Using principal-based authentication for '{}'", name);
                return new ACIJwtAuthProvider(
                        hostUrl,
                        credential.getPrincipalId(),
                        jwtServiceAccessor.get().get(),
                        registrationServiceAccessor.get().get(),
                        httpClientProvider,
                        eventPublisher);
            } else {
                throw new IllegalStateException("Unable to load jwtService via jwtServiceAccessor");
            }
        }

        @Override
        public AuthProvider visit(final UnauthenticatedCredential credential) {
            log.trace("Using no authentication for '{}'", name);
            return new NoAuthAuthProvider(hostUrl, httpClientProvider);
        }
    }
}
