package com.atlassian.jira.plugins.dvcs.bbrebrand;

import javax.inject.Named;

/**
 * Represents dark feature for enabling Bitbucket / Stash rebrand.
 * Dark feature used to be "fusion.bitbucket.rebrand"
 */
@Named
public class BitbucketRebrandDarkFeature {
    /**
     * @return <code>true</code>
     */
    public boolean isBitbucketRebrandEnabled() {
        return true;
    }
}
