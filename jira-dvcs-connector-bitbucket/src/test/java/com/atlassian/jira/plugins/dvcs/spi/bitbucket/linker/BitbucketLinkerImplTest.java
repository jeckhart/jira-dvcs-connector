package com.atlassian.jira.plugins.dvcs.spi.bitbucket.linker;

import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.TwoLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketClientBuilderFactory;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryLink;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryLinkHandler;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.RepositoryLinkRemoteRestpoint;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.collect.ImmutableList;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.linker.BitbucketLinkerImpl.LOWEST_ALLOWED_REGEX_MAX_LEN;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class BitbucketLinkerImplTest {
    static final String THIS_JIRA_URL = "https://jira.example.com/";
    static final String THIS_JIRA_ISSUE_LINK_URL_PATTERN = THIS_JIRA_URL + "browse/\\1";

    static final String EXPECTED_LINKER_REGEX_PREFIX = "(?<!\\w)((";
    static final String EXPECTED_LINKER_REGEX_SUFFIX = ")-\\d+)(?!\\w)";
    static final int REGEX_MAX_SIZE = 250;

    @Mock
    Logger log;

    @Mock
    RepositoryService repositoryService;

    @Mock
    ProjectManager projectManager;

    @Mock
    RepositoryLinkRemoteRestpoint repositoryLinkRemoteRestpoint;

    @Mock
    ApplicationProperties applicationProperties;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    BitbucketClientBuilderFactory builderFactory;

    BitbucketLinkerImpl bitbucketLinker;

    @Mock
    Project project2;

    @Mock
    Project project1;
    List<Project> projectList;

    BitbucketRepositoryLink link1 = createLink(("TEST"));
    BitbucketRepositoryLink link2 = createLink(("ASDF"));
    List<BitbucketRepositoryLink> links;
    List<String> projectKeys = ImmutableList.of("TEST", "ASDF");
    Repository repository;

    @BeforeMethod
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        repository = setUpRepository();
        links = new ArrayList<>();
        links.add(link1);
        links.add(link2);
        projectList = setUpProjectList();
        when(repositoryLinkRemoteRestpoint.getRepositoryLinks
                (repository.getOrgName(), repository.getSlug())).thenReturn(new ArrayList<>());
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn(THIS_JIRA_URL);
        bitbucketLinker = new BitbucketLinkerImpl(
                applicationProperties, projectManager, builderFactory, repositoryService);
        when(builderFactory.forRepository(repository).
                closeIdleConnections().build().getRepositoryLinksRest()).thenReturn(repositoryLinkRemoteRestpoint);
        when(builderFactory.forRepository(repository).
                build().getRepositoryLinksRest()).thenReturn(repositoryLinkRemoteRestpoint);

        when(projectManager.getProjectObjects()).thenReturn(projectList);
    }

    @Test
    public void testUnlinkRepository() throws Exception {
        when(repositoryLinkRemoteRestpoint.getRepositoryLinks
                (repository.getOrgName(), repository.getSlug())).thenReturn(links);
        bitbucketLinker.unlinkRepository(repository);
        verify(repositoryLinkRemoteRestpoint).getRepositoryLinks(repository.getOrgName(), repository.getSlug());
        verify(repositoryLinkRemoteRestpoint).removeRepositoryLink(
                repository.getOrgName(), repository.getSlug(), link1.getId());
        verify(repositoryLinkRemoteRestpoint).removeRepositoryLink(
                repository.getOrgName(), repository.getSlug(), link2.getId());
        verifyNoMoreInteractions(repositoryLinkRemoteRestpoint);
    }

    @Test
    public void testLinkRepositoryRemovalOfExistingKeys() throws Exception {
        when(repositoryLinkRemoteRestpoint.getRepositoryLinks
                (repository.getOrgName(), repository.getSlug())).thenReturn(links);
        final List<String> projectKeysSubset = singletonList(projectKeys.get(0));
        when(repositoryService.getPreviouslyLinkedProjects(repository)).thenReturn(projectKeysSubset);
        bitbucketLinker.linkRepository(repository, new HashSet<>(projectKeys)); //necessary because link repository mutates the set it works on
        verify(repositoryLinkRemoteRestpoint).removeRepositoryLink(
                repository.getOrgName(), repository.getSlug(), link1.getId());
        verify(repositoryLinkRemoteRestpoint).removeRepositoryLink(
                repository.getOrgName(), repository.getSlug(), link2.getId());
    }

    @Test
    public void testLinkRepositoryWhenNoNewKeys() throws Exception {
        when(repositoryService.getPreviouslyLinkedProjects(repository)).thenReturn(projectKeys);
        bitbucketLinker.linkRepository(repository, new HashSet<>(projectKeys)); //necessary because link repository mutates the set it works on
        verify(repositoryLinkRemoteRestpoint, never()).addCustomRepositoryLink(
                anyString(), anyString(), anyString(), anyString());
    }

    @Test
    public void testLinkRepositoryManyProjectKeys() {
        // PREPARE
        final List<Project> projectList = createProjectList("TST", 100); // Create projects
        final Set<String> projectKeys = new HashSet<>();
        projectList.forEach(p -> projectKeys.add(p.getKey())); // Add project keys to 'projectKeys'

        when(repositoryService.getPreviouslyLinkedProjects(repository)).thenReturn(newArrayList());
        when(projectManager.getProjectObjects()).thenReturn(projectList);

        // TEST
        bitbucketLinker.linkRepository(repository, projectKeys);

        // VERIFY
        final ArgumentCaptor<String> urlsCaptor = ArgumentCaptor.forClass(String.class);
        // We expect 3 custom links for BB
        final int expectedCustomLinkCount = 3;
        verify(repositoryLinkRemoteRestpoint, times(expectedCustomLinkCount)).addCustomRepositoryLink(
                eq(repository.getOrgName()),
                eq(repository.getSlug()),
                urlsCaptor.capture(),
                anyString());
        final List<String> urlsCaptorAllValues = urlsCaptor.getAllValues();
        urlsCaptorAllValues.forEach(url ->
        {
            // Every Custom link should start with current JIRA URL Issue link pattern
            assertThat(url, startsWith(THIS_JIRA_ISSUE_LINK_URL_PATTERN));
        });
        // Verify that all urls are unique by adding them to a Set an checking that the size of the set is equal
        // to the size of the original list
        Set<String> urlsUnique = new HashSet<>(urlsCaptorAllValues);
        assertThat(urlsUnique.size(), equalTo(urlsCaptorAllValues.size()));
    }

    @Test
    public void testLinkRepositoryWhenNewKeysInChangesets() throws Exception {
        final List<String> projectKeysSubset = singletonList(projectKeys.get(0));
        when(repositoryService.getPreviouslyLinkedProjects(repository)).thenReturn(projectKeysSubset);
        bitbucketLinker.linkRepository(repository, new HashSet<>(projectKeys));
        verify(repositoryLinkRemoteRestpoint)
                .addCustomRepositoryLink(
                        eq(repository.getOrgName()),
                        eq(repository.getSlug()),
                        eq(THIS_JIRA_ISSUE_LINK_URL_PATTERN),
                        anyString());
    }

    @Test
    public void linkRepository_doesNothing_whenConnectOrganization() throws Exception {
        // setup
        repository.setCredential(new PrincipalIDCredential("HOBO"));

        // execute
        bitbucketLinker.linkRepository(repository, new HashSet<>(projectKeys));

        // check
        verify(repositoryService, never()).getPreviouslyLinkedProjects(any(Repository.class));
    }

    @Test
    public void testConstructProjectsRegexList_returnsEmptyListForNoProjecKeys() {
        final List<String> regexList = bitbucketLinker.constructProjectsRegexList(newArrayList(), REGEX_MAX_SIZE);
        assertThat(regexList, empty());
    }

    @Test
    public void testConstructProjectsRegexList_returnsSingleRegexWhenRegexLengthUnderLimit() {
        final List<String> keys = Arrays.asList("PROJ1", "PROJ2", "PROJ3");
        final List<String> regexList = bitbucketLinker.constructProjectsRegexList(keys, REGEX_MAX_SIZE);
        assertThat(regexList.size(), is(1));
        final String regex = regexList.get(0);
        final String expectedRegex = EXPECTED_LINKER_REGEX_PREFIX + "PROJ1|PROJ2|PROJ3" + EXPECTED_LINKER_REGEX_SUFFIX;
        assertThat(regex, is(expectedRegex));
    }

    @Test
    public void testConstructProjectsRegexList_returnsMultipleRegexsWhenRegexLengthOverLimit() {
        final List<String> keys = Arrays.asList("PROJ1", "PROJ2", "PROJ3", "PROJ4", "PROJ5", "PROJ6");
        final List<String> regexList = bitbucketLinker.constructProjectsRegexList(keys, 35);
        assertThat(regexList.size(), is(3));
        final String expectedRegex1 = EXPECTED_LINKER_REGEX_PREFIX + "PROJ1|PROJ2" + EXPECTED_LINKER_REGEX_SUFFIX;
        final String expectedRegex2 = EXPECTED_LINKER_REGEX_PREFIX + "PROJ3|PROJ4" + EXPECTED_LINKER_REGEX_SUFFIX;
        final String expectedRegex3 = EXPECTED_LINKER_REGEX_PREFIX + "PROJ5|PROJ6" + EXPECTED_LINKER_REGEX_SUFFIX;
        assertThat(regexList, contains(expectedRegex1, expectedRegex2, expectedRegex3));
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testConstructProjectsRegexList_throwsIllegalArgumentExceptionWhenMaxRegexLengthTooLow() {
        final List<String> keys = Arrays.asList("PROJ1", "PROJ2", "PROJ3", "PROJ4", "PROJ5", "PROJ6");
        bitbucketLinker.constructProjectsRegexList(keys, LOWEST_ALLOWED_REGEX_MAX_LEN - 1);
    }

    private BitbucketRepositoryLink createLink(String projectKey) {
        BitbucketRepositoryLink repositoryLink = new BitbucketRepositoryLink();
        repositoryLink.setHandler(createHandler(projectKey));
        repositoryLink.setId(projectKey.hashCode());
        return repositoryLink;
    }

    private BitbucketRepositoryLinkHandler createHandler(String key) {
        BitbucketRepositoryLinkHandler handler = new BitbucketRepositoryLinkHandler();
        handler.setUrl(THIS_JIRA_URL);
        handler.setDisplayTo(THIS_JIRA_URL);
        handler.setName("custom");
        handler.setKey(key);
        return handler;
    }

    private Repository setUpRepository() {
        repository = new Repository();
        repository.setOrgName("some account name");
        repository.setSlug("myrepo");
        repository.setCredential(new TwoLeggedOAuthCredential("key", "secret"));
        return repository;
    }

    private List<Project> setUpProjectList() {
        projectList = new ArrayList<>();
        when(project1.getKey()).thenReturn("ASDF");
        when(project2.getKey()).thenReturn("TEST");
        projectList.add(project1);
        projectList.add(project2);
        return projectList;
    }

    /**
     * Creates new list of Project mocks which will have generated keys based
     * on 'keyPrefix' and a numeric suffix (1 .. projectCount).
     */
    private List<Project> createProjectList(final String keyPrefix, final int projectCount) {
        List<Project> result = newArrayList();
        for (int i = 1; i < projectCount; i++) {
            Project p = mock(Project.class);
            final String key = keyPrefix + i;
            when(p.getKey()).thenReturn(key);
            result.add(p);
        }
        return result;
    }
}