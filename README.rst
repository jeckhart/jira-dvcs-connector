Overview
========

With the Bitbucket and JIRA plugin you can create unlimited private repositories with full-featured issue and project management support. And it takes less than 30 seconds to configure. Bring your code to the next level with Bitbucket and JIRA.

* `30 second overview video`_
* `Install the JIRA DVCS Connector`_

.. image:: http://blog.bitbucket.org/wp-content/uploads/2011/06/Bitbucket-JIRA-tab1.png
    :align: center

Features
========

* Track changesets, monitor source code edits, and link back to Bitbucket.
* Push your changesets to JIRA by referencing issue keys in your commit messages.
* Map unlimited public and private Bitbucket repositories to your JIRA projects. 
* Full setup and configuration in under 1 minute.

.. _`Install the JIRA DVCS Connector`: https://plugins.atlassian.com/plugin/details/311676
.. _`30 second overview video`: http://www.youtube.com/watch?v=7Eeq_87y3NM

Development Guide
=================

Required Environment
--------------------

* Java JDK 8+ (recommend using `jenv <http://www.jenv.be/>`_ to manage JDK versions)
* Maven 3.2+ (recommend using `mvnvm <http://mvnvm.org/>`_ to manage Maven versions)
* NPM 2+ (recommend using `nvm <https://github.com/creationix/nvm>`_ to manage NPM installations)

Issue Tracking
--------------
Issue Tracking for this project has moved to the JSW project, please log tickets there with a component of DVCS Accounts / Smart Commits as appropriate
+-----------------------+----------------------------------------+
| Current               | https://jira.atlassian.com/browse/JSW  |
+-----------------------+----------------------------------------+
| Historical            | https://jira.atlassian.com/browse/DCON |
+-----------------------+----------------------------------------+

Development Branches
--------------------
Basically new work should go on master.

+-----------------+-------------------------------------+-------------------------+
|Branch           | Versions                            | Supported JIRA Versions |
+=================+=====================================+=========================+
| master          | 3.3.x                               | 7.0 and above           |
+-----------------+-------------------------------------+-------------------------+
| jira6.4.x       | 3.2.x                               | 6.4                     |
+-----------------+-------------------------------------+-------------------------+
| jira6.3.x       | 2.1.x                               | 6.3.x                   |
+-----------------+-------------------------------------+-------------------------+
| jira6.2.x       | 2.0.x                               | 6.2.1 ~ 6.2.x           |
+-----------------+-------------------------------------+-------------------------+
| jira5.2.x-6.2   | 1.4.x (excluding 1.4.16 and 1.4.17) | 5.2.x ~ 6.2             |
+-----------------+-------------------------------------+-------------------------+
| jira4.x         | ~ 0.15.13                           | 4.x                     |
+-----------------+-------------------------------------+-------------------------+

Building and running
====================
The integration tests use a specific user account in Bitbucket whose credentials are not available, also they take around an hour to run. If you want to compile the code you can use ```mvn clean install -DskipITs=true``` which will run the unit test suite.

* Build the whole project: ```mvn clean install -DskipITs``` (from the root directory)
* Launch the plugin with QuickReload enabled: ```mvn jira:debug -DskipTests -Pqr``` (from the plugin directory)
* Launch the plugin with an alternate Bitbucket URL: ```mvn jira:debug -DskipTests -Pqr -Dbitbucket.url=http://my.bitbucket.url``` (from the plugin directory)
* Run the JS unit tests: ```npm install && npm test``` (from ```jira-dvcs-connector-plugin/etc/frontend```)

Testing
=======
How to run integration tests against a unicorn JIRA instance and Bitbucket
--------------------------------------------------------------------------
You need to include a ``localDevOnly.properties`` file at ``jira-dvcs-connector-plugin/src/test/resources/localDevOnly.properties``,
which contains config for running the tests in the form of <key>=<value> pairs separated by newlines. For more details on what the contents
of this file should be, contact the Fusion team. Once the file is in place, you should be able to run the integration tests from
the IDE in the normal fashion.


Releasing
=========
Skip the tests as they take too long - ```mvn release:clean release:prepare release:perform -DskipTests -Darguments="-DskipTests" -DautoVersionSubmodules=true```
Typically manual releases are only required for the stable branch, releases from master should occur through the freezer pipeline.