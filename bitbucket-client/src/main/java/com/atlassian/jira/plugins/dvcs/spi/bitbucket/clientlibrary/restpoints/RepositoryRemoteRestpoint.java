package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints;


import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepository;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepositoryEnvelope;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.ResponseCallback;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.ClientUtils.fromJson;

public class RepositoryRemoteRestpoint {
    public static final Type BB_REPOSITORY_TYPE = new TypeToken<BitbucketRepository>() {
    }.getType();
    public static final Type BB_REPOSITORY_ENVELOPE_TYPE = new TypeToken<BitbucketRepositoryEnvelope>() {
    }.getType();

    private final RemoteRequestor requestor;

    public RepositoryRemoteRestpoint(RemoteRequestor requestor) {
        super();
        this.requestor = requestor;
    }

    public List<BitbucketRepository> getAllRepositories(final String owner) {
        String getAllRepositoriesUrl = URLPathFormatter.format("/users/%s", owner);

        return requestor.get(getAllRepositoriesUrl, null, response ->
                ClientUtils.<BitbucketRepositoryEnvelope>fromJson(response.getResponse(), BB_REPOSITORY_ENVELOPE_TYPE).getRepositories());
    }

    public BitbucketRepository getRepository(String owner, String repositorySlug) {
        String getRepositoryUrl = URLPathFormatter.format("/repositories/%s/%s", owner, repositorySlug);

        return requestor.get(getRepositoryUrl, null, response -> fromJson(response.getResponse(), BB_REPOSITORY_TYPE));
    }

    public void removeRepository(String owner, String repositoryName) {
        Map<String, String> removeRepoPostData = new HashMap<>();
        removeRepoPostData.put("repo_slug", repositoryName);
        removeRepoPostData.put("accountname", owner);

        String removeRepositoryUrl = String.format("/repositories/%s/%s", owner, repositoryName);

        requestor.delete(removeRepositoryUrl, removeRepoPostData, ResponseCallback.EMPTY);
    }

    public BitbucketRepository forkRepository(String owner, String repositoryName, String newRepositoryName, boolean isPrivate) {
        Map<String, String> createRepoPostData = new HashMap<>();
        createRepoPostData.put("name", newRepositoryName);
        createRepoPostData.put("is_private", Boolean.toString(isPrivate));

        String forkRepositoryUrl = String.format("/repositories/%s/%s/fork", owner, repositoryName);

        return requestor.post(forkRepositoryUrl, createRepoPostData, response -> fromJson(response.getResponse(), BB_REPOSITORY_TYPE));
    }

    public BitbucketRepository createRepository(String repositoryName, String scm, boolean isPrivate) {
        Map<String, String> createRepoPostData = new HashMap<>();
        createRepoPostData.put("name", repositoryName);
        createRepoPostData.put("scm", scm);
        createRepoPostData.put("is_private", Boolean.toString(isPrivate));

        return requestor.post("/repositories", createRepoPostData, response -> fromJson(response.getResponse(), BB_REPOSITORY_TYPE));
    }

    public BitbucketRepository createRepository(String owner, String repositoryName, String scm, boolean isPrivate) {
        Map<String, String> createRepoPostData = new HashMap<>();
        createRepoPostData.put("name", repositoryName);
        createRepoPostData.put("scm", scm);
        createRepoPostData.put("is_private", Boolean.toString(isPrivate));

        String createRepositoryUrl = String.format("/repositories/%s/%s", owner, repositoryName);

        return requestor.put(createRepositoryUrl, createRepoPostData, response -> fromJson(response.getResponse(), BB_REPOSITORY_TYPE));
    }

    public void createHgRepository(String repositoryName) {
        createRepository(repositoryName, ScmType.HG, true);
    }

    public void createGitRepository(String repositoryName) {
        createRepository(repositoryName, ScmType.GIT, true);
    }

    public static class ScmType {
        public static final String HG = "hg";
        public static final String GIT = "git";
    }
}

