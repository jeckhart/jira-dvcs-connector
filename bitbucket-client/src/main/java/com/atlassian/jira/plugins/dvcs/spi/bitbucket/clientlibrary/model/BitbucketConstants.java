package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

/**
 * @author Martin Skurla
 */
public interface BitbucketConstants {
    String REPOSITORY_LINK_TYPE_JIRA = "jira";
    String REPOSITORY_LINK_TYPE_CUSTOM = "custom";

    String BITBUCKET_CONNECTOR_APPLICATION_ID = "jira-bitbucket-connector-plugin";
}
