package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.plugins.dvcs.model.Flag;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import org.apache.http.auth.AuthenticationException;

import javax.annotation.Nonnull;
import java.io.IOException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Utility class for accessing the DVCS connector internal User resource
 */
public class UserClient extends DvcsRestClient<UserClient> {
    public UserClient(@Nonnull JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    public void setUserHasSeenFeatureDiscovery(boolean hasSeenFeatureDiscovery) throws IOException,
            AuthenticationException {

        Flag flag = new Flag(hasSeenFeatureDiscovery);

        createInternalResource().path("user/flag/featureDiscoverySeen").type(APPLICATION_JSON_TYPE).put(flag);
    }

    public boolean hasUserSeenFeatureDiscovery() throws IOException, AuthenticationException {
        Flag flag = createInternalResource()
                .path("user/flag/featureDiscoverySeen")
                .type(APPLICATION_JSON_TYPE)
                .get(Flag.class);
        return flag.getValue();
    }
}
