define('jira-dvcs-connector/bitbucket/views/confirmation-dialog', [
    'jquery',
    'jira-dvcs-connector/lib/backbone'
], function ($,
             Backbone) {
    "use strict";

    var ESC_KEY_CODE = 27;
    // just wrap the same public method confirmation dialog into a backbone view
    // should change this to use aui dialog2
    // should write unit test for this module
    return Backbone.View.extend({

        initialize: function (options) {
            var dialog = new AJS.Dialog({
                width: options.width ? options.width : 500,
                height: options.height ? options.height : 150,
                id: options.id ? options.id : "confirm-dialog",
                closeOnOutsideClick: !!options.closeOnOutsideClick
            });

            dialog.addHeader(options.header);
            dialog.addPanel("ConfirmPanel", options.body + "<div id='aui-message-bar-confirmation-dialog'></div>");

            dialog.addButtonPanel();
            dialog.page[0].buttonpanel.append("<span id='confirm-action-wait' class='aui-icon' style='padding-right:10px'>&nbsp;</span>");

            dialog.addSubmit(options.submitButtonLabel, function (dialog, event) {
                dialog.working(true);
                if (typeof options.okAction == 'function') {
                    options.okAction(dialog);
                }
            });

            dialog.addCancel("Cancel", function (dialog) {
                if (typeof options.cancelAction == 'function') {
                    options.cancelAction(dialog);
                }
                dialog.remove();
            }, "#");

            dialog.disableActions = function () {
                $('#confirm-dialog .button-panel-submit-button').attr("disabled", "disabled");
                $('#confirm-dialog .button-panel-submit-button').attr("aria-disabled", "true");
                $('#confirm-dialog .button-panel-cancel-link').addClass('dvcs-link-disabled');
            }

            dialog.enableActions = function () {
                $('#confirm-dialog .button-panel-submit-button').removeAttr("disabled");
                $('#confirm-dialog .button-panel-submit-button').removeAttr("aria-disabled");
                $('#confirm-dialog .button-panel-cancel-link').removeClass('dvcs-link-disabled');
            }

            dialog.working = function (working) {
                if (working) {
                    $("#confirm-action-wait").addClass("aui-icon-wait");
                    this.disableActions();
                } else {
                    $("#confirm-action-wait").removeClass("aui-icon-wait");
                    this.enableActions();
                }
            }

            dialog.showError = function (message) {
                dialog.working(false);
                showError(message, "#aui-message-bar-confirmation-dialog");
                dialog.updateHeight();
            }

            dialog.isAttached = function () {
                return !$.isEmptyObject(dialog.popup.element);
            }

            AJS.bind("show.dialog", this._onShow.bind(this));
            AJS.bind("hide.dialog", this._unbindPreventEscCloseHandler.bind(this));
            AJS.bind("remove.dialog", this._unbindPreventEscCloseHandler.bind(this));

            this.dialog = dialog;
        },

        render: function () {
            this.dialog.show();
            this.dialog.updateHeight();
        },

        _preventEscClose: function (e) {
            if (e.keyCode == ESC_KEY_CODE) {
                e.preventDefault();
                e.stopPropagation();
            }
        },

        _onShow: function (e, data) {
            if (data.dialog == this.dialog && !this.dialog.closeOnOutsideClick) {
                AJS.$('*').bind('keydown', this._preventEscClose);
            }
        },

        _unbindPreventEscCloseHandler: function (e, data) {
            if (data.dialog == this.dialog && !this.dialog.closeOnOutsideClick) {
                AJS.$('*').unbind('keydown', this._preventEscClose);
            }
        }
    });
});


