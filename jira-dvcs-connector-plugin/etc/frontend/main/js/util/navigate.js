/**
 * A utility module that wraps interactions with the browser location.
 *
 * Abstracts away from the underlying window object which makes client code more testable, and provides utility methods
 * for working with the window location (event-safe redirect etc.). Utilities include:
 * <ul>
 * <li> simple fragment manipulation API that can retrieve and set the fragment part, along with
 *   fragment params that can be added to the fragment without causing a backend page request etc.
 *   e.g. http://myurl.com#urlHash?hashParam=value
 * </li>
 * <li> the ability to append an element id to the url after a double hash, and then retrieve this id later so the
 *   element can be scrolled to after a refresh
 *   e.g. http://myurl.com##elementId
 * </li>
 * </ul>
 *
 * @module jira-dvcs-connector/util/navigate
 */
define('jira-dvcs-connector/util/navigate', [
    'underscore',
    'wrm/context-path',
    'jira-dvcs-connector/util/window'
], function (_, contextPath, window) {
    "use strict";

    return {
        /**
         * @returns the current URL that is being displayed by the browser
         */
        getUrl: function () {
            return window.location.href;
        },

        /**
         * @returns {string} The origin of the current page
         */
        getOrigin: function () {
            if (window.location.origin) {
                return window.location.origin;
            }
            return window.location.protocol + "//" + window.location.host;
        },

        /**
         * @returns {string} The hash/fragment component of the current window location (if there is one), with hash
         * params etc. stripped from it e.g. "http://example.com/path#hash?key=value" -> "hash"
         */
        getHash: function () {
            if (!window.location.hash) {
                return "";
            }
            var hash = window.location.hash.indexOf('#');
            var query = window.location.hash.indexOf('?');

            return window.location.hash.substring(hash + 1, query > 0 ? query : window.location.hash.length);
        },

        /**
         * Get the hash params for the current window location, if there are any
         *
         * Supports a hash of the form "#hash?key=value&foo=bar" -> {"key":"value", "foo": "bar"}
         *
         * @returns {{}} The map of key=value params encoded in the current window hash (if any)
         */
        getHashParams: function () {
            if (!window.location.hash) {
                return {};
            }

            var queryStart = window.location.hash.indexOf('?');
            if (queryStart == -1) {
                return {};
            }

            return _parseParams(window.location.hash.substring(queryStart + 1));
        },

        /**
         * Set the url hash to the hash string provided.
         *
         * e.g. http://abc.com#hash
         *
         * @param hash {string} string to set the url hash to.
         */
        setHash: function (hash) {
            window.location.hash = hash;
        },

        /**
         * Set the url hash to ##<elementId>, so that we can retrieve this id from the url after we refresh the page,
         * and then scroll to it.
         *
         * Just setting the url hash to #<elementId> scrolls the browser
         * to that element, but there are some cross browser issues with that approach, and this is more reliable.
         *
         * @param elementId {string} the id of an element on the page to scroll to after navigation
         */
        setIdToScrollPostRefresh: function (elementId) {
            window.location.hash = "##" + elementId;
        },

        /**
         * Retrieve the id after a double hash in the url, if it exists, as this represents an element we want
         * to scroll to, assuming we have just refreshed the page.
         *
         * @returns {string, null} the id of the element to scroll to, or null if we haven't set one.
         */
        getIdToScrollToPostRefreshIfExists: function () {
            if (window.location.hash && window.location.hash.substring(0, 2) === "##") {
                return window.location.hash.substring(2);
            } else {
                return null;
            }
        },

        /**
         * Clear the hash component from the window URL
         */
        clearHash: function () {
            window.location.hash = '';
        },

        /**
         * @returns {{}} The current query params as a hash
         */
        getQueryParams: function () {
            if (!parseUri) {
                // TODO: Implement our own parsing here if needed
                return {};
            }

            return parseUri(window.location.href).queryKey || {};
        },

        /**
         * Turns the supplied hash into a query parameter string, i.e. ? followed by & separated parameters.
         * @param params A hash of params
         * @returns String An empty string if the hash is empty or a query string with the params encoded
         */
        buildQueryParams: function (params) {
            var queryParams = [];
            for (var param in params) {
                if (param && params.hasOwnProperty(param)) {
                    queryParams.push(encodeURI(param) + '=' + encodeURI(params[param]));
                }
            }

            return queryParams.length === 0 ? '' : '?' + queryParams.join('&');
        },

        /**
         * Triggers browser reload of the page that is currently displayed
         */
        reload: function () {
            _.defer(function () {
                window.location.reload();
            });
        },

        /**
         * Make the browser navigate to specified URL after all current events have been processed.
         * @param url to navigate to
         */
        navigate: function (url) {
            _.defer(function () {
                window.location = url;
            });
        },

        /**
         * Redirects user to JIRA login page. After successful login, user will be redirected to the currently
         * displayed page.
         */
        redirectToLogin: function () {
            this.navigate(contextPath() + '/login.jsp?permissionViolation=true&os_destination=' +
                encodeURIComponent(window.location.href));
        }
    };

    /**
     * Parse and return parameters from the provided query part
     *
     * @param queryPart The query part to parse - of the form "key=value&foo=bar"
     * @returns {{}}
     * @private
     */
    function _parseParams(queryPart) {
        if (!queryPart) {
            return {}
        }
        var result = {};
        _.each(queryPart.split('&'), function (q) {
            if (!q) {
                return;
            }
            var kv = q.split('=');
            result[kv[0]] = kv[1];
        });
        return result;
    }
});
