/**
 * AMD wrapper around the aui trigger mechanism. Prefixed with dvcs as aui/trigger seems to conflict with something in
 * aui
 *
 * @module jira-dvcs-connector/aui/trigger
 */
define('jira-dvcs-connector/aui/trigger', [], function () {
    return AJS.trigger;
});