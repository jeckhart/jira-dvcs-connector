/**
 * Functions that add behaviour to the Configure Organizations screen.
 * Methods in this module are being migrated here from bitbucket.js
 *
 * @module jira-dvcs-connector/admin/configure-organization
 */
define('jira-dvcs-connector/admin/configure-organization',
    ['require',
        'wrm/context-path',
        'jira-dvcs-connector/aui/messages',
        'jquery',
        'jira-dvcs-connector/util/console',
        'jira-dvcs-connector/util/navigate',
        'jira-dvcs-connector/util/settings',
        'jira-dvcs-connector/admin/view/refresh-account-dialog',
        'jira-dvcs-connector/bitbucket/views/connection-successful',
        'jira-dvcs-connector/feature-discovery/feature-discovery-view',
        'jira-dvcs-connector/rest/dvcs-connector-rest-client',
        'jira-dvcs-connector/analytics/analytics-client',
        'jira-dvcs-connector/admin/dvcs-notifications'],
    function (require,
              contextPath,
              Messages,
              $,
              console,
              Navigate,
              LocalSettingsAccessor,
              RefreshAccountDialog,
              ConnectionSuccessfulDialog,
              FeatureDiscovery,
              DvcsConnectorRestClient,
              AnalyticsClient,
              Notification) {

    var restClient = new DvcsConnectorRestClient(contextPath());
    var analyticsClient = new AnalyticsClient();

    return {

        /**
         * Execute the "complete connection" flow.
         *
         * This will:
         *
         * 1. Launch the "Connection Successful" dialog and allow users to set the org's auto settings
         * 2. Refresh the Org's repository list
         * 3. Show the "Feature Discovery" tour if applicable
         * 4. Refresh the page on completion to ensure data is up-to-date
         *
         * @param {int} organizationId The ID of the organization to complete
         * @param {string} [organizationName] The organization name to display. If not provided, will retrieve from the page.
         */
        doFinishConnectionFlow: function(organizationId, organizationName) {
            var self = this;
            if (!self._isValidOrganizationId(organizationId)) {
                console.log('No organization found with ID ' + organizationId);
                return;
            }

            if (!organizationName) {
                organizationName = self._findOrganizationName(organizationId);
            }

            // Launch the connection successful dialog
            var connectionSuccessfulTask = self._showConnectionSuccessDialog(organizationId, organizationName);

            // When it has completed (including updating the auto settings), trigger the
            // repository refresh and feature discovery tour if needed
            connectionSuccessfulTask.done(function() {
                var refreshRepositoryListTask = restClient.organization.refreshRepositoryList(organizationId);

                self.showFeatureDiscovery()
                    .always(function() {
                        self._attachUIElementsToRepositoryRefreshTask(refreshRepositoryListTask, organizationId, organizationName);
                    });
            });
        },

        /**
         * Trigger a sync of the repository list for the given org.
         *
         * Will launch a dialog+spinner during refresh.
         *
         * @param {int} organizationId
         * @param {string} [organizationName]
         */
        syncRepositoryList: function (organizationId, organizationName) {
            var self = this;
            if (!this._isValidOrganizationId(organizationId)) {
                console.log('No organization found with ID ' + organizationId);
                return;
            }

            if (!organizationName) {
                organizationName = this._findOrganizationName(organizationId);
            }

            var refreshRepositoryListTask = restClient.organization.refreshRepositoryList(organizationId);
            self._attachUIElementsToRepositoryRefreshTask(refreshRepositoryListTask, organizationId, organizationName);
        },

        /**
         * Show the Connection Successful dialog for the provided organization and then trigger refresh of the
         * organization repository list on successful completion of the dialog.
         *
         * On 'submit' will update the autolink/smart commits settings and trigger a refresh of the
         * organization's repository list.
         *
         * @param {int} organizationId The ID of the organization to refresh
         * @param {string} [organizationName] The name of the organization to display.
         *  If not provided will be retrieved from the DOM.
         *
         * @return {Deferred} A deferred object representing the display of the connection successful dialog.
         */
        showConnectionSuccessDialogAndRefresh: function (organizationId, organizationName) {
            var self = this;
            if (!this._isValidOrganizationId(organizationId)) {
                console.log('No organization found with ID ' + organizationId);
                return $.Deferred().reject();
            }

            if (!organizationName) {
                organizationName = this._findOrganizationName(organizationId);
            }

            return this._showConnectionSuccessDialog(organizationId, organizationName).done(function() {
                self.syncRepositoryList(organizationId, organizationName);
            });
        },

        /**
         * Shows the feature discovery tour to the user.
         *
         * The returned deferred will always resolve. On completion it will include a flag indicating if the tour
         * was completed or not.
         *
         * @return {Deferred} a deferred object that will complete when the user dismisses the dialog
         */
        showFeatureDiscovery: function () {
            var deferred = $.Deferred();

            var localSettings = new LocalSettingsAccessor();
            if (!localSettings.shouldShowFeatureDiscovery()) {
                deferred.resolve(false);
                return deferred;
            }

            var featureDiscovery = new FeatureDiscovery();
            featureDiscovery
                .on("tourFinished", function(){
                    analyticsClient.fireFeatureDiscoveryCompleted();
                })
                .on("tourAborted", function(){
                    analyticsClient.fireFeatureDiscoveryAborted(featureDiscovery.getCurrentPageIndex());
                })
                .on("closed", function(){
                    localSettings.setFeatureDiscoveryShown();
                    restClient.internal.user.setHasUserSeenFeatureDiscovery(true).always(function(){
                        deferred.resolve(true);
                    });
                });
            analyticsClient.fireFeatureDiscoveryStarted();
            featureDiscovery.show();

            return deferred;
        },

        /**
         * Work out which organization we should scroll to based on the id in the query hash or query params
         * @returns {el} A jquery element that is the element that should be scrolled to
         */
        getOrganizationElementToScrollTo: function () {
            var id = Navigate.getIdToScrollToPostRefreshIfExists();
            if (id) {
                return $('#' + id);
            }

            var queryParams = Navigate.getQueryParams();
            var queryPrincipalUuid = queryParams.principalUuid;
            if (queryPrincipalUuid) {
                var principalDataSelector = '[data-org-principal-id="' + queryPrincipalUuid + '"]';
                var organizationElement = $(principalDataSelector);
                analyticsClient.fireConnectOrganizationNavigatedByPrincipal(organizationElement.attr('id'),
                    queryPrincipalUuid);
                return organizationElement;
            }
        },

        /**
         * Show an AUI message where the message handling is specified in the callback.
         *
         * @param {string} message The message to show the user
         * @param {string} [auiMessageElement] The selector for the DOM element to inject the message into
         * @param {boolean} [closeable] Whether the message is closable or not
         * @param callback callback to apply to render the message, this will be set to Messages
         *
         * @private
         */
        _showAuiMessage: function (message, auiMessageElement, closeable, callback) {
            if (typeof auiMessageElement == 'undefined') {
                auiMessageElement = "#aui-message-bar-global";
            }

            $(auiMessageElement).empty();
            callback.call(Messages, auiMessageElement, {
                title: message,
                closeable: closeable
            });
        },

        /**
         * Returns if the provided ID matches an organization currently rendered on the page.
         *
         * Note that this method does not do any backend calls and should be used only after page render.
         *
         * @param {int} organizationId The ID to check
         * @returns {boolean} true if the provided ID matches an organization currently rendered on the page; false otherwise.
         *
         * @private
         */
        _isValidOrganizationId: function (organizationId) {
            var $orgContainer = $('#dvcs-orgdata-container-' + organizationId);
            return $orgContainer.length > 0;
        },

        /**
         * Find the organization name for the organization with the given ID.
         *
         * Will query the DOM.
         *
         * @param {int} organizationId The ID of the organization to find the name for
         * @returns {string|null} The organization name, or null if none is found.
         *
         * @private
         */
        _findOrganizationName: function (organizationId) {
            var $orgContainer = $('#dvcs-orgdata-container-' + organizationId);
            if ($orgContainer.length === 0) {
                return null;
            }
            return $orgContainer.data('org-name');
        },

        /**
         * Show the Connection Successful dialog for the provided organization.
         *
         * On 'submit' will update the autolink/smart commits settings for the org based on the user's selections.
         *
         * Returns a deferred object that is tied to the lifecycle of the dialog. This deferred will be rejected if the
         * settings update operation fails ("fail"); otherwise it will be resolved ("done").
         *
         * @param {int} organizationId The ID of the organization to refresh. Assumed to be pre-validated and valid.
         * @param {string} [organizationName] The name of the organization to display.
         *  If not provided will be retrieved from the DOM.
         *
         * @return {Deferred} A deferred object representing the display of the connection successful dialog
         *
         * @private
         */
        _showConnectionSuccessDialog: function (organizationId, organizationName) {
            var $deferred = $.Deferred();
            var self = this;
            var connectionSuccessDialog = new ConnectionSuccessfulDialog({
                organizationId: organizationId,
                organizationName: organizationName,
                baseUrl: contextPath(),
                onSuccess: function () {
                    Navigate.setIdToScrollPostRefresh("dvcs-orgdata-container-" + organizationId);
                    $deferred.resolve();
                },
                onError: function () {
                    Notification.showError(
                        AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.error.unexpected'),
                        "#aui-message-bar-" + organizationId);
                    $deferred.reject();
                }
            });
            connectionSuccessDialog.render();

            return $deferred;
        },

        /**
         * Attach UI elements to a running refresh repository list task.
         *
         * This will bind a RefreshAccountDialog to the task, and trigger a page refresh on completion of the task.
         * In the case of the refresh task failing, will show an appropriate error/warning on the UI.
         *
         * @param refreshRepositoryListTask The task to attach the UI elements to
         * @param organizationId The organization id being refreshed
         * @param organizationName The organization name to display
         * @private
         */
        _attachUIElementsToRepositoryRefreshTask: function (refreshRepositoryListTask, organizationId, organizationName) {
            var self = this;

            // Avoid showing the dialog if the task has already failed...
            if ("rejected" === refreshRepositoryListTask.state()) {
                self._showRefreshFailedMessage(refreshRepositoryListTask.statusText, organizationId, organizationName);
                return;
            }

            // Otherwise attach the dialog to the running task...
            //
            // Note that this creates a nicer user experience even if the task has completed
            // it will take some time for the page to refresh, and showing the dialog even if the REST call has
            // completed gives the user feedback that the page refresh is under way.
            var dialog = new RefreshAccountDialog({
                organizationName: organizationName
            });
            dialog.show();

            refreshRepositoryListTask
                .done(function() {
                    Navigate.reload();
                })
                .fail(function (jqXHR, statusText, errorThrown) {
                    self._showRefreshFailedMessage(statusText, organizationId, organizationName);
                    dialog.hide();
                });
        },

        /**
         * Show the refresh list failed message based.
         *
         * @param statusText The status of the failed task. Used to determine what message to show.
         * @param organizationId The ID of the org the refresh failed for
         * @param organizationName The name of the org the refresh failed for
         * @private
         */
        _showRefreshFailedMessage: function(statusText, organizationId, organizationName) {
            if ("timeout" === statusText) {
                Notification.showWarning(
                    AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.error.refresh-timed-out', organizationName),
                    "#aui-message-bar-" + organizationId);
            } else {
                Notification.showError(
                    AJS.I18n.getText('com.atlassian.jira.plugins.dvcs.admin.error.refresh-failed', organizationName),
                    "#aui-message-bar-" + organizationId);
            }
        }
    }
});