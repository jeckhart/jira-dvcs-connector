/**
 * Warning and error notifications for use with the configure accounts screen
 *
 * @module jira-dvcs-connector/admin/dvcs-notifications
 */
define('jira-dvcs-connector/admin/dvcs-notifications',
    ['require'],
    function (require) {
        "use strict";

        var $ = require("jquery");
        var Messages = require('jira-dvcs-connector/aui/messages');

        return {
            /**
             * Show an AUI error message on the DVCS Accounts admin page
             *
             * @param {string} message The message to show the user
             * @param {string} [auiMessageElement] The selector for the DOM element to inject the message into
             * @param {boolean} [closeable] Whether the message is closable or not
             */
            showError: function (message, auiMessageElement, closeable) {
                this.showAuiMessage(message, auiMessageElement, closeable, Messages.error);
            },

            /**
             * Show an AUI warning message on the DVCS Accounts admin page
             *
             * @param {string} message The message to show the user
             * @param {string} [auiMessageElement] The selector for the DOM element to inject the message into
             * @param {boolean} [closeable] Whether the message is closable or not
             */
            showWarning: function (message, auiMessageElement, closeable) {
                this.showAuiMessage(message, auiMessageElement, closeable, Messages.warning);
            },

            /**
             * Show an AUI message where the message handling is specified in the callback.
             *
             * @param {string} message The message to show the user
             * @param {string} [auiMessageElement] The selector for the DOM element to inject the message into
             * @param {boolean} [closeable] Whether the message is closable or not
             * @param callback callback to apply to render the message, this will be set to Messages
             */
            showAuiMessage: function (message, auiMessageElement, closeable, callback) {
                if (typeof auiMessageElement == 'undefined') {
                    auiMessageElement = "#aui-message-bar-global";
                }

                $(auiMessageElement).empty();
                callback.call(Messages, auiMessageElement, {
                    title: message,
                    closeable: closeable
                });
            }
        };
    }
);
