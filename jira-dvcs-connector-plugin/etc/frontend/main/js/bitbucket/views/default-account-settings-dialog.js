/**
 * The 'default account setting' dialog shown to configure default setting
 *
 * @module jira-dvcs-connector/bitbucket/views/default-account-setting
 */
define('jira-dvcs-connector/bitbucket/views/default-account-setting', ['require'], function (require) {
    "use strict";

    var dialog2 = require('jira-dvcs-connector/aui/dialog2');
    var configDialog = require('jira-dvcs-connector/bitbucket/views/repo-config-dialog');
    var AnalyticsClient = require('jira-dvcs-connector/analytics/analytics-client');
    var analyticsClient = new AnalyticsClient();

    /**
     * A view that will render the Connection Successful dialog into the page and show it.
     */
    return configDialog.extend({

        template: JIRA.Templates.DVCSConnector.Bitbucket.configureAccoutSettingsDialog,
        events: {
            'click #dialog-submit-button': '_submit',
            'click #dialog-close-button': '_closeDialog'
        },

        /**
         * Initialize the view. Invoked on construction of a new view instance.
         *
         * @param {Object} options - Initialization options
         * @param {int} options.organizationId - The ID of the organization the dialog is for
         * @param {string} options.organizationName - The organization name to display on the dialog
         * @param {boolean} options.autoLinkEnabled - Are auto linking of repos enabled for the account
         * @param {boolean} options.smartCommitsEnabled - Are smart commits enabled by default for the account
         * @param {function} options.onSuccess - A callback to invoke on successful update of
         *  autolink/smartcommits settings
         * @param {function} options.onError - A callback to invoke on failed update of autolink/smartcommits settings
         */
        initialize: function (options) {
            this.orgId = options.organizationId;
            this.onSuccess = options.onSuccess;
            this.onError = options.onError;

            this.dialog = dialog2(this.template({
                organizationName: options.organizationName,
                autoLinkEnabled: options.autoLinkEnabled,
                smartCommitsEnabled: options.smartCommitsEnabled
            }));
            this.$cancelButton = this.$el.find('#dialog-close-button');
            this._initialize();

        },

        _submit: function () {
            this.submitUpdates(analyticsClient.fireDefaultSettingsChanged, true);
        },

        _closeDialog: function () {
            AJS.dialog2("#default-account-settings-dialog").hide();
        }
    });
});