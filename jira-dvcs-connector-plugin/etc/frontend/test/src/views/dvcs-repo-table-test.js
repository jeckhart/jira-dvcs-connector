define([
    'fusion/test/qunit',
    'fusion/test/hamjest',
    'fusion/test/backbone'
], function (QUnit,
             __,
             Backbone) {

    const RepRowViewMock = Backbone.Model.extend({});

    const repoRowViewOnSpy = sinon.spy(RepRowViewMock.prototype, 'initialize');

    const RepoModelMock = Backbone.Model.extend({
        idAttribute: 'id',
        isEnabled() {return this.get('linked')}
    });

    const RepoCollectionMock = Backbone.Collection.extend({
        model: RepoModelMock,
        filterEnabled() {},
        indexOf: () => 0
    });

    let repoRow = '<tr></tr>';
    const templateSpy = sinon.stub();

    let filterEnabledStub;

    QUnit.module('jira-dvcs-connector/bitbucket/views/dvcs-repos-table-view', {
        require: {
            main: 'jira-dvcs-connector/bitbucket/views/dvcs-repos-table-view',
            backbone: 'fusion/test/backbone'
        },
        mocks: {
            backbone: QUnit.moduleMock('jira-dvcs-connector/lib/backbone', () => Backbone),
            RepRowView: QUnit.moduleMock('jira-dvcs-connector/bitbucket/views/dvcs-repo-row-view', () => RepRowViewMock)
        },

        templates: {
            'dvcs.connector.plugin.soy.dvcs.accounts.repoRow': templateSpy
        },

        beforeEach: function (assert, TableView) {
            templateSpy.returns(repoRow);
            this.fixture.append('<table><tbody></tbody></table>');
        },

        afterEach: function () {
            repoRowViewOnSpy.reset();
            filterEnabledStub && filterEnabledStub.restore();
            templateSpy.reset();
            RepoCollectionMock.prototype.on.restore && RepoCollectionMock.prototype.on.restore();
            RepRowViewMock.prototype.destroy.restore && RepRowViewMock.prototype.destroy.restore();
        }
    });

    QUnit.test('should listen to repo collection changes', function(assert, RepoTable) {
        const collection = new RepoCollectionMock({});
        const eventRegisterSpy = sinon.spy(RepoCollectionMock.prototype, 'on');

        new RepoTable({el: 'table', collection: collection});

        assert.assertThat('table view should listen for repo collection link changes', eventRegisterSpy.called, __.equalTo(true));
        assert.assertThat('table view should register listener for collection change with listener function', eventRegisterSpy.withArgs(sinon.match(event => 'change:linked' == event),
            sinon.match( callback => typeof callback == 'function')).calledOnce, __.equalTo(true));
    });


    QUnit.test('should only render enabled repos', function (assert, RepoTable) {
        const collection = new RepoCollectionMock();
        const repo = new Backbone.Model({id: 10, linked: true, name: 'repo1'});
        const repo2 = new Backbone.Model({id: 700, linked: true, name: 'repo700'});
        filterEnabledStub = sinon.stub(RepoCollectionMock.prototype, 'filterEnabled', () => new RepoCollectionMock([repo, repo2]));

        const tableView = new RepoTable({el: 'table', collection: collection});
        tableView.render();

        assert.assertThat('should call collection to filter only enabled repos', filterEnabledStub.callCount, __.greaterThan(1));
        assert.assertThat('should use repo template to render repo row', templateSpy.callCount, __.equalTo(2));
        assert.assertThat('should create instance of RepoRowView per row', repoRowViewOnSpy.callCount, __.equalTo(2));
        assert.assertThat('should use repo template to render repo row', templateSpy.firstCall.args[0], __.hasProperties({
            repo: {
                id: 10,
                linked: true,
                name: 'repo1'
            }
        }));
        assert.assertThat('should render repo1 row in tbody', this.fixture.find('tbody').find('tr').length, __.equalTo(2));
    });

    QUnit.test('should add repo row to dom on enable repo', function (assert, RepoTable) {
        const repo = new RepoModelMock({
            id: 101,
            name: 'repo101',
            linked: false
        });
        const collection = new RepoCollectionMock([repo]);
        const filterEnabledStub = sinon.stub(RepoCollectionMock.prototype, 'filterEnabled', () => new RepoCollectionMock([]));
        const tableView = new RepoTable({el: 'table', collection: collection});
        tableView.render();

        assert.assertThat('should call collection to filter only enabled repos', filterEnabledStub.called, __.equalTo(true));
        assert.assertThat('should not render any rows when there is no enabled repos', templateSpy.callCount, __.equalTo(0));
        assert.assertThat('should not find any repo row in tbody', this.fixture.find('tbody').find('tr').length, __.equalTo(0));

        repo.set({linked: true});

        assert.assertThat('should render the new added repos', templateSpy.callCount, __.equalTo(1));
        assert.assertThat('should create instance of RepoRowView for new added repo', repoRowViewOnSpy.callCount, __.equalTo(1));
        assert.assertThat('should use repo template to render the new added repo', templateSpy.firstCall.args[0], __.hasProperties({
            repo: {
                id: 101,
                name: 'repo101',
                linked: true
            }
        }));
        assert.assertThat('should render new added repo row in tbody', this.fixture.find('tbody').find('tr').length, __.equalTo(1));
    });

    QUnit.test('should remove repo row from dom when repo is disabled', function(assert, RepoTable) {
        const repo = new RepoModelMock({
            id: 101,
            name: 'repo101',
            linked: true
        });
        // the enabled row id
        repoRow = '<tr id="dvcs-repo-row-101"></tr>';
        templateSpy.returns(repoRow);
        const collection = new RepoCollectionMock([repo]);
        const filterEnabledStub = sinon.stub(RepoCollectionMock.prototype, 'filterEnabled', () => new RepoCollectionMock([repo]));
        const eventRegisterSpy = sinon.spy(RepoCollectionMock.prototype, 'on');
        const tableView = new RepoTable({el: 'table', collection: collection});
        tableView.render();

        assert.assertThat('should call collection to filter only enabled repos', filterEnabledStub.called, __.equalTo(true));
        assert.assertThat('should render enabled row', templateSpy.callCount, __.equalTo(1));
        assert.assertThat('should create instance of RepoRowView for initially enabled repo', repoRowViewOnSpy.callCount, __.equalTo(1));
        assert.assertThat('should initially render enabled row tbody', this.fixture.find('tbody').find('tr').length, __.equalTo(1));

        const repoRowDestroySpy = sinon.spy(RepRowViewMock.prototype, 'destroy');

        repo.set({linked: false});

        assert.assertThat('should destroy repo row view for removed repo', repoRowDestroySpy.callCount, __.equalTo(1));
        assert.assertThat('should remove repo row from dom when it is disabled', this.fixture.find('tbody').find('tr').length, __.equalTo(0));
    });

    QUnit.test('Should add new repo if it is linked and remove deleted repo if it was linked', function(assert, RepoTable) {
        let filterEnabledStubImpl = () => new RepoCollectionMock([]);
        // create empty collection
        const collection = new RepoCollectionMock([]);

        const tableView = new RepoTable({el: 'table', collection: collection});
        filterEnabledStub = sinon.stub(RepoCollectionMock.prototype, 'filterEnabled', filterEnabledStubImpl);
        tableView.render();

        assert.assertThat('should not render any row', templateSpy.callCount, __.equalTo(0));
        assert.assertThat('should has 0 rendered rows initially', this.fixture.find('tbody').find('tr').length, __.equalTo(0));

        const repo1 = new RepoModelMock({
            id: 101,
            name: 'repo101',
            linked: true
        });

        const repo2 = new RepoModelMock({
            id: 202,
            name: 'repo202',
            linked: true
        });

        const repo3 = new RepoModelMock({
            id: 333,
            name: 'repo333',
            linked: false
        });

        filterEnabledStubImpl = () => new RepoCollectionMock([repo1, repo2]);

        templateSpy.onFirstCall().returns('<tr id="dvcs-repo-row-101"></tr>');
        templateSpy.onSecondCall().returns('<tr id="dvcs-repo-row-202"></tr>');
        templateSpy.onThirdCall().returns('<tr id="dvcs-repo-row-333"></tr>');

        collection.add([repo1, repo2, repo3]);

        assert.assertThat('should render linked repos only', templateSpy.callCount, __.equalTo(2));
        assert.assertThat('should contain two rows for two linked repos', this.fixture.find('tr').length, __.equalTo(2));
        assert.assertThat('should not render linked repos only',
            [this.fixture.find('tr#dvcs-repo-row-101').length, this.fixture.find('tr#dvcs-repo-row-202').length,
                this.fixture.find('tr#dvcs-repo-row-333').length], __.equalTo([1, 1, 0]));

        const repoRowDestroySpy = sinon.spy(RepRowViewMock.prototype, 'destroy');

        collection.remove(repo1);
        collection.remove(repo3);

        assert.assertThat('should remove only rendered repo', repoRowDestroySpy.callCount, __.equalTo(1));

        assert.assertThat('should not render undeleted linked repos only',
            [this.fixture.find('tr#dvcs-repo-row-101').length, this.fixture.find('tr#dvcs-repo-row-202').length,
                this.fixture.find('tr#dvcs-repo-row-333').length], __.equalTo([0, 1, 0]));
    });
});