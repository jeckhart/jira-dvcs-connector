package it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.ondemand.JiraOdLoginPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import it.util.TestRule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.plugins.dvcs.util.PasswordUtil.getPassword;
import static com.google.common.base.Preconditions.checkNotNull;
import static it.util.TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;

/**
 * Log into Jira and Bitbucket prior to test execution
 */
public class ProductLoginRule extends TestRule {
    private final JiraTestedProduct jira;
    private final BitbucketTestedProduct bitbucket;
    private final String jiraUsername;
    private final String jiraPassword;

    public ProductLoginRule(
            @Nonnull final JiraTestedProduct jira,
            @Nonnull final BitbucketTestedProduct bitbucket,
            @Nullable final String jiraUsername,
            @Nullable final String jiraPassword) {
        this.jira = checkNotNull(jira);
        this.bitbucket = checkNotNull(bitbucket);
        this.jiraUsername = jiraUsername;
        this.jiraPassword = jiraPassword;
    }

    @Override
    protected void doApply() throws Exception {
        jira.goTo(JiraOdLoginPage.class).loginAndGoToHome(jiraUsername, jiraPassword);

        bitbucket.login(JIRA_BB_CONNECTOR_ACCOUNT, getPassword(JIRA_BB_CONNECTOR_ACCOUNT));
    }
}
