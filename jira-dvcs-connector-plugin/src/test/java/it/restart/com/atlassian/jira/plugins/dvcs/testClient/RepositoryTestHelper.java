package it.restart.com.atlassian.jira.plugins.dvcs.testClient;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.base.resource.TimestampNameTestResource;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.rest.OrganizationClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketRepository;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.RepositoryRemoteRestpoint;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A helper class to work with a randomly named repository, holds some relevant state to allow for cleanup in tests.
 * <p>
 * Concrete implementations of this class will be specialised to a particular remote provider (Bitbucket, Github,
 * Github Enterprise) and Dvcs type (Git, Mercurial). The methods are expected to be used as follows:
 * <ul>
 * <li>#initialiseOrganizationsAndDvcs will be called during the once per class initialisation</li>
 * <li>#deleteAllOrganizations will be called during the once per class teardown</li>
 * <li>#setupTestRepository will be called per test method to create a repository</li>
 * <li>#cleanupLocalRepositories will be called per test method to remove created repositories</li>
 * </ul>
 */
public abstract class RepositoryTestHelper<T extends TestedProduct<WebDriverTester>> {
    protected final Collection<BitbucketRepository> testRepositories = new ArrayList<>();
    protected final JiraTestedProduct jiraTestedProduct;
    protected final OrganizationClient organizationClient;
    protected final String userName;
    protected final String password;
    protected final T dvcsProduct;

    protected Dvcs dvcs;
    protected OAuth oAuth;

    protected RepositoryTestHelper(@Nonnull final String userName, @Nonnull final String password,
                                   @Nonnull final JiraTestedProduct jiraTestedProduct, @Nonnull final T dvcsProduct) {
        this.dvcsProduct = checkNotNull(dvcsProduct);
        this.jiraTestedProduct = checkNotNull(jiraTestedProduct);
        this.organizationClient = new OrganizationClient(jiraTestedProduct.environmentData());
        this.password = checkNotNull(password);
        this.userName = checkNotNull(userName);
    }

    /**
     * Create a DVCS Organization and dvcs instance to use. Expects the user to already be logged into JIRA.
     *
     * @param dvcs  the dvcs to use, if not provided then it will be instantiated, see #getDvcs
     * @param oauth the oAuth token, if not provided then it will be created for the relevant provider, see #getoAuth
     */
    public abstract void initialiseOrganizationsAndDvcs(@Nonnull Optional<Dvcs> dvcs, @Nonnull Optional<OAuth> oauth);

    /**
     * Removes all organizations that are associated, implementing classes should also delete any OAuth tokens or other
     * specific configuration.
     */
    public void deleteAllOrganizations() {
        organizationClient.deleteAll();
    }

    /**
     * Create a repository with the given name, usually from a {@link TimestampNameTestResource}.
     *
     * @param repositoryName the repo name
     */
    public abstract void setupTestRepository(String repositoryName);

    /**
     * Cleanup any created repositories, will also attempt to remove repositories
     * that are 'expired' as judged by the {@link TimestampNameTestResource}.
     *
     * @param timestampNameTestResource the timestamp name strategy
     */
    public abstract void cleanupLocalRepositories(TimestampNameTestResource timestampNameTestResource);

    /**
     * Deletes the given OAuth consumer.
     *
     * @param oAuth The OAuth details to delete
     */
    public abstract void deleteOAuthConsumer(OAuth oAuth);

    protected final void removeExpiredRepositories(final TimestampNameTestResource timestampNameTestResource) {
        final BitbucketRemoteClient bbRemoteClient = new BitbucketRemoteClient(userName, password);
        final RepositoryRemoteRestpoint repositoryService = bbRemoteClient.getRepositoriesRest();

        for (final BitbucketRepository repository : repositoryService.getAllRepositories(userName)) {
            if (timestampNameTestResource.isExpired(repository.getName())) {
                try {
                    repositoryService.removeRepository(userName, repository.getName());
                }
                // TODO do not catch BitbucketRequestException.ServiceUnAvailable_503  after deleting faulty repo FUSE-3276
                // https://bitbucket.org/jirabitbucketconnector/bitbucketprtest_82_1445475891695
                catch (BitbucketRequestException.NotFound_404 | BitbucketRequestException.ServiceUnAvailable_503 ignored) {
                } // the repo does not exist
            }
        }
    }

    public Dvcs getDvcs() {
        return dvcs;
    }

    public OAuth getOAuth() {
        return oAuth;
    }
}
