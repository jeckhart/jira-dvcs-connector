package it.restart.com.atlassian.jira.plugins.dvcs.test.pullRequest;

import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketPullRequest;
import it.restart.com.atlassian.jira.plugins.dvcs.testClient.BitbucketPRClient;
import it.restart.com.atlassian.jira.plugins.dvcs.testClient.BitbucketRepositoryTestHelper;
import it.restart.com.atlassian.jira.plugins.dvcs.testClient.RepositoryTestHelper;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import static it.restart.com.atlassian.jira.plugins.dvcs.test.IntegrationTestUserDetails.ACCOUNT_NAME;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.IntegrationTestUserDetails.PASSWORD;
import static it.restart.com.atlassian.jira.plugins.dvcs.testClient.DvcsType.GIT;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class BitbucketPRTest extends PullRequestTestCases<BitbucketPullRequest> {
    protected BitbucketTestedProduct bitbucket;
    private RepositoryTestHelper repositoryTestHelper;

    @BeforeClass
    public void beforeClass() {
        setUpEnvironment();
    }

    @AfterMethod
    public void afterMethod() {
        deleteCreatedIssues();
    }

    @Override
    protected void beforeEachTestClassInitialisation(final JiraTestedProduct jiraTestedProduct) {
        bitbucket = new BitbucketTestedProduct(getJiraTestedProduct().getTester());
        repositoryTestHelper = new BitbucketRepositoryTestHelper(
                ACCOUNT_NAME, PASSWORD, getJiraTestedProduct(), bitbucket, GIT);
        repositoryTestHelper.initialiseOrganizationsAndDvcs(empty(), empty());

        this.dvcs = repositoryTestHelper.getDvcs();
        this.oAuth = repositoryTestHelper.getOAuth();

        final RepositoryTestHelper forkRepositoryTestHelper = new BitbucketRepositoryTestHelper(
                FORK_ACCOUNT_NAME, FORK_ACCOUNT_PASSWORD, getJiraTestedProduct(), bitbucket, GIT);
        forkRepositoryTestHelper.initialiseOrganizationsAndDvcs(ofNullable(dvcs), ofNullable(oAuth));

        MockitoAnnotations.initMocks(this);
        final MockComponentWorker mockComponentWorker = new MockComponentWorker().init();
        mockComponentWorker.getMockApplicationProperties().setEncoding("US-ASCII");
        pullRequestClient = new BitbucketPRClient();
    }

    @Override
    protected void cleanupAfterClass() {
        repositoryTestHelper.deleteOAuthConsumer(oAuth);
    }

    @Override
    protected void initLocalTestRepository() {
        repositoryTestHelper.setupTestRepository(repositoryName);
    }

    @Override
    protected void cleanupLocalTestRepository() {
        repositoryTestHelper.cleanupLocalRepositories(timestampNameTestResource);
    }

    @Override
    protected Account.AccountType getAccountType() {
        return Account.AccountType.BITBUCKET;
    }
}
