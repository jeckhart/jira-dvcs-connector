package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.plugins.dvcs.pageobjects.bitbucket.BitbucketGrantAccessPage;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.BitbucketTestedProduct;
import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.OAuthCredentials;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.Account.AccountType;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.AccountOAuthDialog;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.account.DvcsAccountsPage;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.client.BitbucketRemoteClient;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.restpoints.ConsumerRemoteRestpoint;
import it.com.atlassian.jira.plugins.dvcs.DvcsWebDriverTestCase;
import it.util.TestAccounts;
import org.json.JSONWriter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileWriter;

import static com.atlassian.jira.plugins.dvcs.ondemand.JsonFileBasedAccountsConfigProvider.ENV_ONDEMAND_CONFIGURATION;
import static com.atlassian.jira.plugins.dvcs.ondemand.JsonFileBasedAccountsConfigProvider.ENV_ONDEMAND_CONFIGURATION_DEFAULT;
import static com.atlassian.jira.plugins.dvcs.util.PasswordUtil.getPassword;
import static com.google.common.base.Preconditions.checkNotNull;
import static it.util.TestUtils.toOAuth;
import static java.lang.String.format;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests the integrated accounts feature.
 *
 * @author Stanislav Dvorscak
 */
public class IntegratedAccountsTest extends DvcsWebDriverTestCase {
    private static final String BB_ACCOUNT_NAME = TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT;

    private static final BitbucketRemoteClient BITBUCKET_CLIENT =
            new BitbucketRemoteClient(BB_ACCOUNT_NAME, getPassword(BB_ACCOUNT_NAME));

    private static final BitbucketTestedProduct BITBUCKET = new BitbucketTestedProduct(JIRA.getTester());
    private OAuth oAuthOriginal;
    private OAuth oAuthNew;
    private String onDemandConfigurationPath;

    /**
     * Creates the given file and its parent directories if they don't already exist.
     *
     * @param file the file to create
     * @return the file
     * @throws Exception
     */
    @Nonnull
    private static File createIfNotExists(@Nonnull final File file) throws Exception {
        if (!file.getParentFile().exists()) {
            check(file.getParentFile().mkdirs(), "Could not create path '{}'", file.getParentFile());
        }
        if (!file.exists()) {
            check(file.createNewFile(), "Could not create file '{}'", file);
        }
        return file;
    }

    private static void assertIsOnDemand(final Account account) {
        check(account.isIntegratedAccount(), "Provided account has to be integrated account/OnDemand account!");
    }

    /**
     * Asserts the given flag to be true.
     *
     * @param value   the value to check is true
     * @param message the message to show if it's false
     * @param args    any format arguments to the given message
     */
    private static void check(final boolean value, final String message, final Object... args) {
        assertThat(value).as(format(message, args)).isTrue();
    }

    @BeforeClass
    public void prepareCommonEnvironment() {
        JIRA.quickLoginAsAdmin();

        oAuthOriginal = toOAuth(BITBUCKET_CLIENT.getConsumerRemoteRestpoint().createConsumer(BB_ACCOUNT_NAME));
        oAuthNew = toOAuth(BITBUCKET_CLIENT.getConsumerRemoteRestpoint().createConsumer(BB_ACCOUNT_NAME));

        BITBUCKET.login(BB_ACCOUNT_NAME, getPassword(BB_ACCOUNT_NAME));

        onDemandConfigurationPath = System.getProperty(ENV_ONDEMAND_CONFIGURATION, ENV_ONDEMAND_CONFIGURATION_DEFAULT);
    }

    @AfterClass(alwaysRun = true)
    public void tearDownCommonEnvironment() {
        final ConsumerRemoteRestpoint consumerRemoteRestpoint = BITBUCKET_CLIENT.getConsumerRemoteRestpoint();
        consumerRemoteRestpoint.deleteConsumer(BB_ACCOUNT_NAME, oAuthOriginal.applicationId);
        consumerRemoteRestpoint.deleteConsumer(BB_ACCOUNT_NAME, oAuthNew.applicationId);
    }

    @BeforeMethod
    public void prepareTestEnvironment() throws Exception {
        removeAllIntegratedAccounts();
        deleteAllOrganizations();
    }

    @AfterMethod(alwaysRun = true)
    public void destroyTestEnvironment() throws Exception {
        removeAllIntegratedAccounts();
        deleteAllOrganizations();
    }

    /**
     * Tests that OAuth of an account is regenerated/changed correctly.
     */
    @Test
    public void testEditOAuth() {
        addOrganization(
                com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType.BITBUCKET, BB_ACCOUNT_NAME,
                new OAuthCredentials(oAuthOriginal.key, oAuthOriginal.secret), false);

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(AccountType.BITBUCKET, BB_ACCOUNT_NAME);
        account.regenerate().regenerate(oAuthNew.key, oAuthNew.secret);

        if (JIRA.getTester().getDriver().getCurrentUrl().startsWith(BitbucketDetails.getHostUrl())) {
            JIRA.getPageBinder().bind(BitbucketGrantAccessPage.class).grantAccess();
        }

        final AccountOAuthDialog oAuthDialog = account.regenerate();
        assertThat("oauth key should be changed to the new key", oAuthNew.key, equalTo(oAuthDialog.getKey()));
        assertTrue(oAuthDialog.getAuthorizingAccountAccountUrl().endsWith(BB_ACCOUNT_NAME));
    }

    @Test
    public void testAddNewIntegratedAccount() throws Exception {
        // Set up
        buildOnDemandProperties(new IntegratedAccount(BB_ACCOUNT_NAME, oAuthOriginal.key, oAuthOriginal.secret));

        // Invoke
        reloadAccountsConfiguration();

        // Check
        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(AccountType.BITBUCKET, BB_ACCOUNT_NAME);
        assertIsOnDemand(account);
    }

    @Test
    public void testChangeExistingAccountToIntegratedAccount() throws Exception {
        addOrganization(
                com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType.BITBUCKET, BB_ACCOUNT_NAME,
                new OAuthCredentials(oAuthOriginal.key, oAuthOriginal.secret), false);

        buildOnDemandProperties(new IntegratedAccount(BB_ACCOUNT_NAME, oAuthNew.key, oAuthNew.secret));
        reloadAccountsConfiguration();

        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        final Account account = accountsPage.getAccount(AccountType.BITBUCKET, BB_ACCOUNT_NAME);
        assertIsOnDemand(account);
    }

    private void removeAllIntegratedAccounts() throws Exception {
        // Set up
        buildOnDemandProperties();

        // Invoke
        reloadAccountsConfiguration();

        // Check
        final DvcsAccountsPage accountsPage = JIRA.visit(DvcsAccountsPage.class);
        accountsPage.getAccounts().forEach(account -> assertThat(account.isIntegratedAccount()).isFalse());
    }

    /**
     * Builds the ondemand.properties file for the given accounts.
     *
     * @param accounts the integrated accounts from which to build the file
     */
    private void buildOnDemandProperties(final IntegratedAccount... accounts) throws Exception {
        final File onDemandConfigFile = createIfNotExists(new File(onDemandConfigurationPath));

        try (final FileWriter onDemandConfigWriter = new FileWriter(onDemandConfigFile)) {
            final JSONWriter json = new JSONWriter(onDemandConfigWriter);

            // root
            json.object();

            // root/sysadmin-application-links[]
            json.key("sysadmin-application-links").array();

            // root/sysadmin-application-links[]/bitbucket[]
            json.object();
            json.key("bitbucket").array();

            for (final IntegratedAccount account : accounts) {
                json.object();
                json.key("account").value(account.name);
                json.key("key").value(account.key);
                json.key("secret").value(account.secret);
                json.endObject();
            }

            // end: root/sysadmin-application-links[]/bitbucket[]
            json.endArray();
            json.endObject();

            // end: root/sysadmin-application-links[]
            json.endArray();

            // end: root
            json.endObject();
        }
    }

    private static final class IntegratedAccount {
        private final String name;
        private final String key;
        private final String secret;

        /**
         * Constructor.
         *
         * @param name   the account name
         * @param key    the appropriate OAuth key of the account
         * @param secret the appropriate OAuth secret of the account
         */
        public IntegratedAccount(final String name, final String key, final String secret) {
            this.name = checkNotNull(name);
            this.key = checkNotNull(key);
            this.secret = checkNotNull(secret);
        }

        @Override
        public String toString() {
            return reflectionToString(this, SHORT_PREFIX_STYLE);
        }
    }
}
