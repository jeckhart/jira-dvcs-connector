package it.restart.com.atlassian.jira.plugins.dvcs.test.aci;

import com.atlassian.fusion.aci.api.feature.AciDarkFeatures;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import it.restart.com.atlassian.jira.plugins.dvcs.test.aci.rule.CheckPluginsEnabledPrecondition;
import org.testng.annotations.BeforeClass;

import java.util.Stack;

public class BaseAciDependentTest {
    private static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);

    private final Stack<Boolean> darkFeatureState = new Stack<>();

    @BeforeClass
    public void checkForAciPlugin() {
        new CheckPluginsEnabledPrecondition(getJira(), false).apply();
    }

    protected void enableDarkFeature() {
        final boolean previousState = getJira().backdoor().darkFeatures().isGlobalEnabled(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        getJira().backdoor().darkFeatures().enableForSite(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        darkFeatureState.push(previousState);
    }

    protected void resetDarkFeature() {
        if (darkFeatureState.pop()) {
            getJira().backdoor().darkFeatures().enableForSite(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        } else {
            getJira().backdoor().darkFeatures().disableForSite(AciDarkFeatures.ACI_ENABLED_FEATURE_KEY);
        }
    }

    protected String getJiraBaseUrl() {
        final String baseUrlExternalForm = getJira().environmentData().getBaseUrl().toExternalForm();
        return baseUrlExternalForm.endsWith("/") ? baseUrlExternalForm : baseUrlExternalForm + "/";
    }

    protected JiraTestedProduct getJira() {
        return JIRA;
    }
}
