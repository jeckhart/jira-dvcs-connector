package it.com.atlassian.jira.plugins.dvcs.event;

import com.atlassian.jira.plugins.dvcs.rest.DvcsRestClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

/**
 * REST client for the {@code bitbucket/1.0/event/limits} resource.
 */
public class LimitsClient extends DvcsRestClient<LimitsClient> {
    public LimitsClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
    }

    private static Map<String, Integer> asHashMap(final Map<String, Integer> map) {
        return new HashMap<>(map);
    }

    public LimitsResponse getLimits() {
        return toLimitsResponse(createResource().get(ClientResponse.class));
    }

    public LimitsResponse setLimits(Map<String, Integer> limits) {
        return toLimitsResponse(createResource().type(MediaType.APPLICATION_JSON).put(ClientResponse.class, asHashMap(limits)));
    }

    @Override
    protected WebResource createResource() {
        return super.createResource().path("event/limits");
    }

    private LimitsResponse toLimitsResponse(final ClientResponse clientResponse) {
        return new LimitsResponse() {
            @Override
            public int status() {
                return clientResponse().getStatus();
            }

            @Override
            public Map<String, Integer> limits() {
                if (clientResponse().getStatus() != 200) {
                    throw new IllegalStateException("response status is " + clientResponse().getStatus());
                }

                return clientResponse.getEntity(new GenericType<Map<String, Integer>>() {
                });
            }

            @Override
            public ClientResponse clientResponse() {
                return clientResponse;
            }
        };
    }

}
