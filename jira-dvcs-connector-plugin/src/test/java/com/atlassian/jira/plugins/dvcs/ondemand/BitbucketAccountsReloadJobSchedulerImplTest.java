package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static com.atlassian.jira.plugins.dvcs.ondemand.BitbucketAccountsReloadJobSchedulerImpl.JOB_ID;
import static com.atlassian.jira.plugins.dvcs.ondemand.BitbucketAccountsReloadJobSchedulerImpl.JOB_RUNNER_KEY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class BitbucketAccountsReloadJobSchedulerImplTest {
    @Mock
    private BitbucketAccountsConfigService mockAccountsConfigService;
    @Mock
    private BitbucketAccountsReloadJobRunner mockJobHandler;
    @Mock
    private SchedulerService mockScheduler;

    private BitbucketAccountsReloadJobSchedulerImpl reloadJobScheduler;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        reloadJobScheduler = new BitbucketAccountsReloadJobSchedulerImpl(mockScheduler, mockJobHandler);
    }

    @Test
    public void registeringJobHandlerShouldDelegateToScheduler() {
        // Invoke
        reloadJobScheduler.registerJobHandler();

        // Check
        verify(mockScheduler).registerJobRunner(JOB_RUNNER_KEY, mockJobHandler);
    }

    @Test
    public void unregisteringJobHandlerShouldDelegateToScheduler() {
        // Invoke
        reloadJobScheduler.unregisterJobHandler();

        // Check
        verify(mockScheduler).unregisterJobRunner(JOB_RUNNER_KEY);
    }

    @Test
    public void schedulingAlreadyScheduledJobShouldDoNothing() {
        // Set up
        final JobDetails mockJobDetails = mock(JobDetails.class);
        when(mockScheduler.getJobDetails(JOB_ID)).thenReturn(mockJobDetails);

        // Invoke
        reloadJobScheduler.schedule();

        // Check
        verify(mockScheduler).getJobDetails(JOB_ID);
        verifyNoMoreInteractions(mockScheduler);
    }

    @Test
    public void schedulingUnscheduledJobShouldScheduleIt() throws Exception {
        // Set up
        when(mockScheduler.getJobDetails(JOB_ID)).thenReturn(null);

        // Invoke
        reloadJobScheduler.schedule();

        // Check
        final ArgumentCaptor<JobConfig> jobConfigCaptor = ArgumentCaptor.forClass(JobConfig.class);
        verify(mockScheduler).scheduleJob(eq(JOB_ID), jobConfigCaptor.capture());
        final JobConfig jobConfig = jobConfigCaptor.getValue();
        assertThat(jobConfig.getJobRunnerKey(), is(JOB_RUNNER_KEY));
        assertThat(jobConfig.getRunMode(), is(RUN_ONCE_PER_CLUSTER));
        final Schedule schedule = jobConfig.getSchedule();
        assertThat(schedule.getType(), is(Schedule.Type.INTERVAL));
        final IntervalScheduleInfo intervalSchedule = schedule.getIntervalScheduleInfo();
        assertThat(intervalSchedule.getFirstRunTime(), is(notNullValue(Date.class)));
    }
}
