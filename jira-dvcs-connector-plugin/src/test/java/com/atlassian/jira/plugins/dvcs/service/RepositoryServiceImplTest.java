package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestDao;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.SmartCommitsAnalyticsService;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.dao.SyncAuditLogDao;
import com.atlassian.jira.plugins.dvcs.event.CarefulEventService;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException.PostCommitHookRegistrationException;
import com.atlassian.jira.plugins.dvcs.model.DefaultProgress;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryRegistration;
import com.atlassian.jira.plugins.dvcs.model.credential.BasicAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.plugins.dvcs.spi.github.service.GitHubEventService;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadPoolExecutor;

import static com.atlassian.jira.plugins.dvcs.service.RepositoryServiceImpl.SYNC_REPOSITORY_LIST_LOCK;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RepositoryServiceImplTest {
    private static final String PRINCIPAL_UUID = "principal uuid";
    private static final String DVCS_TYPE = "bitbucket";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DvcsCommunicatorProvider dvcsCommunicatorProvider;

    @Mock
    private RepositoryDao repositoryDao;

    @Mock
    private RepositoryPullRequestDao repositoryPullRequestDao;

    @Mock
    private BranchService branchService;

    @Mock
    private Synchronizer synchronizer;

    @Mock
    private ChangesetService changesetService;

    @Mock
    private LinkerService linkerService;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private DvcsCommunicator bitbucketCommunicator;

    @Mock
    private PluginSettingsFactory settings;

    @Mock
    private SyncAuditLogDao syncAudit;

    @Mock
    private GitHubEventService gitHubEventService;

    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private CarefulEventService eventService;

    @Mock
    private AnalyticsService analyticsService;

    @Mock
    private Repository repository;

    @Mock
    private SmartCommitsAnalyticsService smartCommitsAnalyticsService;

    @Mock
    private AciInstallationServiceAccessor aciInstallationServiceAccessor;

    @Mock
    private ACIInstallationService aciInstallationService;

    @Mock
    private OrganizationDao organizationDao;

    @Mock
    private ThreadPoolExecutor repositoryDeletionExecutor;

    @Mock
    private ThreadPoolExecutor webhookCleanupExecutor;

    @Mock
    private DvcsConnectorExecutorFactory executorFactory;

    // tested object
    @InjectMocks
    private RepositoryServiceImpl repositoryService;

    @Before
    public void setup() throws Exception {
        setAciNotAvailable();

        when(executorFactory.createRepositoryDeletionThreadPoolExecutor()).thenReturn(repositoryDeletionExecutor);
        when(executorFactory.createWebhookCleanupThreadPoolExecutor()).thenReturn(webhookCleanupExecutor);

        repository = createSampleRepository();

        when(dvcsCommunicatorProvider.getCommunicator(DVCS_TYPE)).thenReturn(bitbucketCommunicator);
        when(bitbucketCommunicator.getUser(eq(repository), anyString())).thenAnswer(i ->
                new DvcsUser((String) i.getArguments()[1], "", "", "", ""));
        when(synchronizer.getProgress(anyInt())).thenReturn(new DefaultProgress());

        repositoryService.init();
    }

    @Test
    public void testDestroy() throws Exception {
        repositoryService.destroy();

        verify(executorFactory).shutdownExecutor(RepositoryServiceImpl.class.getName(), repositoryDeletionExecutor);
        verify(executorFactory).shutdownExecutor(RepositoryServiceImpl.class.getName(), webhookCleanupExecutor);
    }

    @Test
    public void testDisableRepository() {
        Repository sampleRepository = createSampleRepository();
        setRepositoryExists(sampleRepository, true);
        setUpJiraBaseUrl();
        Organization org = createSampleOrganization();
        when(organizationDao.get(sampleRepository.getOrganizationId())).thenReturn(org);


        RepositoryRegistration registration = repositoryService.enableRepository(sampleRepository.getId(), false);

        verify(repositoryDao).save(sampleRepository);
        verify(bitbucketCommunicator).removePostcommitHook(eq(sampleRepository),
                eq(createPostcommitUrl(sampleRepository)));
        assertFalse(registration.isCallBackUrlInstalled());
        assertEquals(registration.getRepository(), sampleRepository);
    }

    @Test
    public void testDisableRepositoryWithoutAdminRights() {
        final Repository sampleRepository = createSampleRepository();
        setRepositoryExists(sampleRepository, true);
        Organization org = createSampleOrganization();
        when(organizationDao.get(sampleRepository.getOrganizationId())).thenReturn(org);

        doThrow(new PostCommitHookRegistrationException("", null)).when(bitbucketCommunicator).removePostcommitHook(any(Repository.class), anyString());
        setUpJiraBaseUrl();

        RepositoryRegistration registration = repositoryService.enableRepository(sampleRepository.getId(), false);

        verify(repositoryDao).save(sampleRepository);
        verify(bitbucketCommunicator).removePostcommitHook(eq(sampleRepository),
                eq(createPostcommitUrl(sampleRepository)));
        assertTrue(registration.isCallBackUrlInstalled());
        assertEquals(registration.getRepository(), sampleRepository);
    }

    @Test
    public void testEnableRepository() {

        Repository sampleRepository = createSampleRepository();
        setRepositoryExists(sampleRepository, true);
        setUpJiraBaseUrl();
        Organization org = createSampleOrganization();
        when(organizationDao.get(sampleRepository.getOrganizationId())).thenReturn(org);

        RepositoryRegistration registration = repositoryService.enableRepository(sampleRepository.getId(), true);

        verify(repositoryDao).save(sampleRepository);
        verify(bitbucketCommunicator).ensureHookPresent(eq(sampleRepository),
                eq(createPostcommitUrl(sampleRepository)));
        assertTrue(registration.isCallBackUrlInstalled());
        assertNotNull(registration.getCallBackUrl());
        assertEquals(registration.getRepository(), sampleRepository);
    }

    @Test
    public void testEnableRepositoryWithoutAdminRights() {

        Repository sampleRepository = createSampleRepository();
        setRepositoryExists(sampleRepository, true);
        doThrow(new PostCommitHookRegistrationException("", null))
                .when(bitbucketCommunicator).ensureHookPresent(any(Repository.class), anyString());
        setUpJiraBaseUrl();
        Organization org = createSampleOrganization();
        when(organizationDao.get(sampleRepository.getOrganizationId())).thenReturn(org);

        RepositoryRegistration registration = repositoryService.enableRepository(sampleRepository.getId(), true);

        verify(repositoryDao).save(sampleRepository);
        verify(bitbucketCommunicator).ensureHookPresent(eq(sampleRepository),
                eq(createPostcommitUrl(sampleRepository)));
        assertFalse(registration.isCallBackUrlInstalled());
        assertNotNull(registration.getCallBackUrl());
        assertEquals(registration.getRepository(), sampleRepository);
    }

    /**
     * Tests that Organization will not be synced if it's ApprovalState is not APPROVED
     */
    @Test
    public void testSyncRepositoryList_nonApprovedOrganizationIsNotSynced() {
        // PREPARE
        final Organization org = createNonApprovedOrganization();
        setOrganizationExists(org, true);

        // TEST
        repositoryService.syncRepositoryList(org);

        // VERIFY
        verifySyncNotPerformed();
    }

    @Test
    public void testSyncRepositoryListNonSoftSync_nonApprovedOrganizationIsNotSynced() {
        // PREPARE
        final Organization org = createNonApprovedOrganization();
        setOrganizationExists(org, true);

        // TEST
        repositoryService.syncRepositoryList(org, false);

        // VERIFY
        verifySyncNotPerformed();
    }

    @Test
    public void testSync_repoIsNotSynced_whenNonApprovedOrganization() {
        // PREPARE
        final Organization org = createNonApprovedOrganization();
        final Repository repo = createSampleRepository(1, org.getId());

        setOrganizationExists(org, true);
        setRepositoryExists(repo, true);

        // TEST
        repositoryService.sync(repo.getId(), EnumSet.of(SynchronizationFlag.SOFT_SYNC));

        // VERIFY
        verifySyncNotPerformed();
    }

    @Test
    public void testSync_repoIsNotSynced_whenNonExistentRepo() {
        // PREPARE
        final Organization org = createNonApprovedOrganization();
        final Repository repo = createSampleRepository(1, org.getId());

        setOrganizationExists(org, true);
        setRepositoryExists(repo, false);

        // TEST
        repositoryService.sync(repo.getId(), EnumSet.of(SynchronizationFlag.SOFT_SYNC));

        // VERIFY
        verifySyncNotPerformed();
    }

    @Test
    public void testSync_repoIsNotSynced_whenRepoIsDeleted() {
        // PREPARE
        final Organization org = createNonApprovedOrganization();
        final Repository repo = createSampleRepository(1, org.getId());
        repo.setDeleted(true);

        setOrganizationExists(org, true);
        setRepositoryExists(repo, true);

        // TEST
        repositoryService.sync(repo.getId(), EnumSet.of(SynchronizationFlag.SOFT_SYNC));

        // VERIFY
        verifySyncNotPerformed();
    }

    @Test
    public void testSync_repoIsSynced_whenValid() {
        // PREPARE
        final Organization org = createApprovedOrganization();
        final Repository repo = createSampleRepository(1, org.getId());

        setOrganizationExists(org, true);
        setRepositoryExists(repo, true);

        // TEST
        repositoryService.sync(repo.getId(), EnumSet.of(SynchronizationFlag.SOFT_SYNC));

        // VERIFY
        verifySyncPerformed(repo);
    }

    @Test
    public void testSyncRepositoryList() {
        when(settings.createGlobalSettings()).thenReturn(mock(PluginSettings.class));

        final Repository sampleRepository1 = createSampleRepository(1, "sampleRepository1");
        final Repository sampleRepository2 = createSampleRepository(2, "sampleRepository2");
        final Repository sampleRepository3 = createSampleRepository(3, "sampleRepository3");
        final Repository sampleRepository4 = createSampleRepository(4, "sampleRepository4");

        // sampleRepository1 and sampleRepository2 local
        final List<Repository> storedRepos = asList(sampleRepository1, sampleRepository2);

        // sampleRepository1 deleted, sampleRepository3 and sampleRepository4 added
        final List<Repository> remoteRepos = asList(sampleRepository2, sampleRepository3, sampleRepository4);

        final Organization sampleOrganization = createSampleOrganization();
        when(organizationDao.get(sampleOrganization.getId())).thenReturn(sampleOrganization);

        when(dvcsCommunicatorProvider.getCommunicator(sampleOrganization.getDvcsType())).thenReturn(bitbucketCommunicator);
        when(bitbucketCommunicator.getRepositories(sampleOrganization, storedRepos)).thenReturn(remoteRepos);

        when(repositoryDao.getAllByOrganization(sampleOrganization.getId(), true)).thenReturn(storedRepos);
        when(repositoryDao.save(sampleRepository3)).thenReturn(sampleRepository3);
        when(repositoryDao.save(sampleRepository4)).thenReturn(sampleRepository4);
        when(repositoryDao.getAllByOrganization(sampleOrganization.getId(), false)).thenReturn(newArrayList(concat(storedRepos, remoteRepos)));

        setUpJiraBaseUrl();

        final ClusterLock mockLock = mock(ClusterLock.class);
        when(clusterLockService.getLockForName(SYNC_REPOSITORY_LIST_LOCK)).thenReturn(mockLock);

        repositoryService.syncRepositoryList(sampleOrganization);

        // sampleRepository2 has been updated
        verify(repositoryDao).save(sampleRepository2);

        // sampleRepository1 has been deleted
        verify(repositoryDao).save(argThat(new ArgumentMatcher<Repository>() {
            @Override
            public boolean matches(Object argument) {
                Repository repo = (Repository) argument;
                return repo.getId() == 1 && repo.isDeleted();
            }
        }));

        // sampleRepository3, sampleRepository4 has been added
        verify(repositoryDao).save(sampleRepository3);
        verify(repositoryDao).save(sampleRepository4);

        // ... with false linking
        verify(bitbucketCommunicator).ensureHookPresent(sampleRepository3, createPostcommitUrl(sampleRepository3));
        verify(bitbucketCommunicator).ensureHookPresent(sampleRepository4, createPostcommitUrl(sampleRepository4));
        verify(mockLock).unlock();
    }

    @Test
    public void remove_removesAllInformation_whenInvoked() {
        final Repository repository = createSampleRepository();

        repositoryService.remove(repository);

        verify(changesetService).removeAllInRepository(repository.getId());
        verify(repositoryDao).remove(repository.getId());
        verify(repositoryPullRequestDao).removeAll(repository);

        verify(bitbucketCommunicator, never()).removePostcommitHook(eq(repository), any());
    }

    @Test
    public void remove_removesPostcommitHook_whenRepositoryIsLinked() {
        final Repository repository = createSampleRepository();
        repository.setLinked(true);

        repositoryService.remove(repository);

        verify(changesetService).removeAllInRepository(repository.getId());
        verify(repositoryDao).remove(repository.getId());
        verify(repositoryPullRequestDao).removeAll(repository);

        verify(bitbucketCommunicator, times(1)).removePostcommitHook(eq(repository), any());
    }

    @Test
    public void remove_doesNotRemovePostcommitHook_whenRepositoryPrincipalBased() {
        final Repository repository = createSampleRepository();
        repository.setLinked(true);
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        repositoryService.remove(repository);

        verify(changesetService).removeAllInRepository(repository.getId());
        verify(repositoryDao).remove(repository.getId());
        verify(repositoryPullRequestDao).removeAll(repository);

        verify(bitbucketCommunicator, never()).removePostcommitHook(eq(repository), any());
    }

    @Test
    public void remove_shouldClearAssociatedEvents() throws Exception {
        repositoryService.remove(repository);
        verify(eventService).discardEvents(repository);
    }

    @Test
    public void getUser_returnsUnknownUser_whenAuthorNull() {
        final DvcsUser user = repositoryService.getUser(repository, null, null);

        verify(bitbucketCommunicator, never()).getUser(any(), any());
        assertTrue(user instanceof DvcsUser.UnknownUser);
    }

    @Test
    public void getUser_returnsUnknownUser_whenAuthorEmptyString() {
        final DvcsUser user = repositoryService.getUser(repository, "", null);

        verify(bitbucketCommunicator, never()).getUser(any(), any());
        assertTrue(user instanceof DvcsUser.UnknownUser);
    }

    @Test
    public void getUser_returnsUser_whenAuthorMatchesUser() {
        final DvcsUser user = repositoryService.getUser(repository, "test", null);

        verify(bitbucketCommunicator, times(1)).getUser(any(), any());
        assertFalse(user instanceof DvcsUser.UnknownUser);
    }

    @Test
    public void getUser_returnsUnknownUser_whenAuthorDoesNotMatchUser() {
        when(bitbucketCommunicator.getUser(any(), any())).thenReturn(null);

        final DvcsUser user = repositoryService.getUser(repository, "test", null);

        verify(bitbucketCommunicator, times(1)).getUser(any(), any());
        assertTrue(user instanceof DvcsUser.UnknownUser);
    }

    @Test
    public void getUser_returnsUnknownUser_whenExceptionOccursReachingRemote() {
        doThrow(Exception.class).when(bitbucketCommunicator).getUser(any(), any());

        final DvcsUser user = repositoryService.getUser(repository, "test", null);

        verify(bitbucketCommunicator, times(1)).getUser(any(), any());
        assertTrue(user instanceof DvcsUser.UnknownUser);
    }

    @Test
    public void enableRepositorySmartcommitsFiresAnalytics() {
        final int ID = 1;
        final boolean isEnabled = true;
        when(repositoryDao.get(ID)).thenReturn(repository);
        repositoryService.enableRepositorySmartcommits(ID, isEnabled);
        verify(smartCommitsAnalyticsService).fireSmartCommitPerRepoConfigChange(ID, isEnabled);
    }

    @Test
    public void shouldRemoveLinkersAndCommitHooks_returnsFalse_ifRepoNotLinked() {
        // setup
        setAciAvailable();

        repository.setLinked(false);
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        createInstallationFor(PRINCIPAL_UUID, Installation.LifeCycleStage.INSTALLED);

        // execute
        boolean result = repositoryService.shouldRemoveLinkersAndCommitHooks(repository);

        // check
        assertFalse("we shouldn't remove linkers and hooks when repo is not linked", result);
    }

    @Test
    public void shouldRemoveLinkersAndCommitHooks_returnsFalse_ifAciInstallationIsAlreadyUninstalled() {
        // setup
        setAciAvailable();

        repository.setLinked(true);
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        createInstallationFor(PRINCIPAL_UUID, Installation.LifeCycleStage.UNINSTALLED);

        // execute
        boolean result = repositoryService.shouldRemoveLinkersAndCommitHooks(repository);

        // check
        assertFalse("we shouldn't remove linkers and hooks when ACI-managed repo is already uninstalled", result);
    }

    @Test
    public void shouldRemoveLinkersAndCommitHooks_returnsTrue_ifRepoNotManagedByAciAndRepoIsLinked() {
        // setup
        setAciAvailable();

        repository.setLinked(true);

        // execute
        boolean result = repositoryService.shouldRemoveLinkersAndCommitHooks(repository);

        // check
        assertTrue("we should remove linkers and hooks when repo is linked and not ACI-managed", result);
    }

    @Test
    public void shouldRemoveLinkersAndCommitHooks_returnsTrue_ifManagedByAciAndNotUninstalledAndRepoIsLinked() {
        // setup
        setAciAvailable();

        repository.setLinked(true);
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        createInstallationFor(PRINCIPAL_UUID, Installation.LifeCycleStage.INSTALLED);

        // execute
        boolean result = repositoryService.shouldRemoveLinkersAndCommitHooks(repository);

        // check
        assertTrue("we should remove linkers and hooks when repo is linked and not ACI-managed", result);
    }

    @Test
    public void shouldRemoveLinkersAndCommitHooks_returnsTrue_ifAciNotAvailableAndRepoIsLinked() {
        // setup
        setAciNotAvailable();

        repository.setLinked(true);
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        // execute
        boolean result = repositoryService.shouldRemoveLinkersAndCommitHooks(repository);

        // check
        assertTrue("we should remove linkers and hooks when repo is linked and not ACI-managed", result);
    }

    @Test
    public void shouldDisableAllRepositories() {
        Organization org = createSampleOrganization();
        when(organizationDao.get(repository.getOrganizationId())).thenReturn(org);

        // Set up
        repository.setLinked(true);
        when(repositoryDao.getAll(false)).thenReturn(singletonList(repository));
        setRepositoryExists(repository, true);

        // Invoke
        repositoryService.disableAllRepositories();

        // Check
        verify(repositoryDao).save(repository);
    }

    @Test(expected = NullPointerException.class)
    public void removePostCommitHook_shouldFail_whenNullRepoProvided() {
        repositoryService.removePostcommitHook(null, false);
    }

    @Test
    public void removePostCommitHook_shouldReturnTrue_whenHookRemoved() {
        final boolean result = repositoryService.removePostcommitHook(repository, false);

        verify(bitbucketCommunicator, times(1)).removePostcommitHook(any(), any());
        assertThat(result, is(true));
    }

    @Test
    public void removePostCommitHook_shouldNotDoAnything_whenPrincipalBasedAndNotForced() {
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        final boolean result = repositoryService.removePostcommitHook(repository, false);

        verify(bitbucketCommunicator, times(0)).removePostcommitHook(any(), any());
        assertThat(result, is(false));
    }

    @Test
    public void removePostCommitHook_shouldIgnoreCredentialType_whenForced() {
        repository.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        final boolean result = repositoryService.removePostcommitHook(repository, true);

        verify(bitbucketCommunicator, times(1)).removePostcommitHook(any(), any());
        assertThat(result, is(true));
    }

    @Test
    public void removePostCommitHook_shouldReturnFalse_whenHookRemovalFailed() {
        doThrow(new IllegalStateException()).when(bitbucketCommunicator).removePostcommitHook(any(), any());

        final boolean result = repositoryService.removePostcommitHook(repository, false);

        verify(bitbucketCommunicator, times(1)).removePostcommitHook(any(), any());
        assertThat(result, is(false));
    }

    @Test(expected = NullPointerException.class)
    public void removePostCommitHooks_shouldFail_whenNullOrgProvided() {
        repositoryService.removePostcommitHooks(null, false);
    }

    @Test
    public void removePostCommitHooks_shouldReturnCountOfSuccessfulRemovals_whenHooksRemoved() {
        final Repository repo1 = createSampleRepository();
        final Repository repo2 = createSampleRepository();
        final Repository repo3 = createSampleRepository();

        final Organization org = createSampleOrganization();

        when(repositoryDao.getAllByOrganization(eq(org.getId()), anyBoolean())).thenReturn(asList(repo1, repo2, repo3));
        doThrow(new IllegalStateException()).when(bitbucketCommunicator).removePostcommitHook(eq(repo2), any());

        final int result = repositoryService.removePostcommitHooks(org, false);

        verify(bitbucketCommunicator, times(3)).removePostcommitHook(any(), any());
        assertThat(result, is(2));
    }

    @Test
    public void removePostCommitHooks_shouldDoNothing_whenNoRepositoriesOnOrg() {
        final Organization org = createSampleOrganization();

        when(repositoryDao.getAllByOrganization(eq(org.getId()), anyBoolean())).thenReturn(emptyList());

        final int result = repositoryService.removePostcommitHooks(org, false);

        verify(bitbucketCommunicator, times(0)).removePostcommitHook(any(), any());
        assertThat(result, is(0));
    }

    @Test
    public void removePostCommitHooks_shouldDoNothing_whenPrincipalBasedOrgAndNotForced() {
        final Repository repo1 = createSampleRepository();
        final Repository repo2 = createSampleRepository();
        final Repository repo3 = createSampleRepository();

        final Organization org = createSampleOrganization();
        org.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        when(repositoryDao.getAllByOrganization(eq(org.getId()), anyBoolean())).thenReturn(asList(repo1, repo2, repo3));

        final int result = repositoryService.removePostcommitHooks(org, false);

        verify(bitbucketCommunicator, times(0)).removePostcommitHook(any(), any());
        assertThat(result, is(0));
    }

    @Test
    public void removePostCommitHooks_shouldRemoveHooks_whenPrincipalBasedOrgAndForced() {
        final Repository repo1 = createSampleRepository();
        final Repository repo2 = createSampleRepository();
        final Repository repo3 = createSampleRepository();

        final Organization org = createSampleOrganization();
        org.setCredential(CredentialFactory.createPrincipalCredential(PRINCIPAL_UUID));

        when(repositoryDao.getAllByOrganization(eq(org.getId()), anyBoolean())).thenReturn(asList(repo1, repo2, repo3));

        final int result = repositoryService.removePostcommitHooks(org, true);

        verify(bitbucketCommunicator, times(3)).removePostcommitHook(any(), any());
        assertThat(result, is(3));
    }

    @Test(expected = NullPointerException.class)
    public void removePostCommitHooksAsync_shouldFail_whenNullOrgProvided() {
        repositoryService.removePostcommitHooksAsync(null, false);
    }

    @Test
    public void removePostCommitHooksAsync_shouldSubmitRemovalJobToExecutor_whenOrgProvided() {
        repositoryService.removePostcommitHooksAsync(createSampleOrganization(), false);

        verify(webhookCleanupExecutor, times(1)).submit(Matchers.<Callable<Integer>>any());
    }

    @SuppressWarnings("deprecation")
    private void setUpJiraBaseUrl() {
        when(applicationProperties.getBaseUrl()).thenReturn("https://myjira.org");
    }

    private void createInstallationFor(final String principalUuid, final Installation.LifeCycleStage lifeCycleStage) {
        final Installation installation = mock(Installation.class);
        when(installation.getLifeCycleStage()).thenReturn(lifeCycleStage);
        when(aciInstallationService.get(BITBUCKET_CONNECTOR_APPLICATION_ID, principalUuid)).thenReturn(installation);
    }

    private void setAciNotAvailable() {
        when(aciInstallationServiceAccessor.get()).thenReturn(Optional.empty());
    }

    private void setAciAvailable() {
        when(aciInstallationServiceAccessor.get()).thenReturn(Optional.of(aciInstallationService));
    }

    private Repository createSampleRepository(final int repositoryId, final int organizationId) {
        return createSampleRepository(repositoryId, RandomStringUtils.random(5), organizationId);
    }

    private Repository createSampleRepository(final int repositoryId, final String name) {
        return createSampleRepository(repositoryId, name, RandomUtils.nextInt());
    }

    private Repository createSampleRepository(final int repositoryId, final String name, final int orgId) {
        final Repository repository = new Repository();
        repository.setName(name);
        repository.setDvcsType("bitbucket");
        repository.setOrganizationId(orgId);
        repository.setSlug(name);
        repository.setCredential(new BasicAuthCredential("user", "pass"));
        repository.setId(repositoryId);
        return repository;
    }

    private Repository createSampleRepository() {
        return createSampleRepository(RandomUtils.nextInt(), RandomUtils.nextInt());
    }

    private Repository createSampleRepository(final int orgId) {
        return createSampleRepository(RandomUtils.nextInt(), orgId);
    }

    private String createPostcommitUrl(Repository forRepo) {
        return "https://myjira.org" + "/rest/bitbucket/1.0/repository/" + forRepo.getId() + "/sync";
    }

    private void setOrganizationExists(final Organization org, final boolean exists) {
        when(organizationDao.get(org.getId())).thenReturn(exists ? org : null);
    }

    private void setRepositoryExists(final Repository repo, final boolean exists) {
        when(repositoryDao.get(repo.getId())).thenReturn(exists ? repo : null);
    }

    private void verifySyncNotPerformed() {
        verify(synchronizer, never()).doSync(any(), any());
    }

    private void verifySyncPerformed(final Repository repository) {
        verify(synchronizer).doSync(eq(repository), any());
    }

    private Organization createNonApprovedOrganization() {
        return createSampleOrganization(RandomUtils.nextInt(), Organization.ApprovalState.PENDING_APPROVAL);
    }

    private Organization createApprovedOrganization() {
        return createSampleOrganization(RandomUtils.nextInt(), Organization.ApprovalState.APPROVED);
    }

    private Organization createSampleOrganization(final int id, final Organization.ApprovalState approvalState) {
        final Organization sampleOrganization = new Organization();
        sampleOrganization.setId(id);
        sampleOrganization.setDvcsType(DVCS_TYPE);
        sampleOrganization.setAutolinkNewRepos(true);
        sampleOrganization.setApprovalState(approvalState);
        return sampleOrganization;
    }

    private Organization createSampleOrganization() {
        final Organization sampleOrganization = new Organization();
        sampleOrganization.setId(5);
        sampleOrganization.setDvcsType("bitbucket");
        sampleOrganization.setAutolinkNewRepos(true);
        sampleOrganization.setCredential(new BasicAuthCredential("user", "pass"));
        return sampleOrganization;
    }
}
