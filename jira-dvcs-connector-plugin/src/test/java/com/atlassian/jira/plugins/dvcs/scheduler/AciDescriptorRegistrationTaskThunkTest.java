package com.atlassian.jira.plugins.dvcs.scheduler;

import com.atlassian.fusion.aci.api.model.ConnectApplication;
import com.atlassian.fusion.aci.api.model.RemoteApplication;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import org.mockito.Mock;
import org.osgi.framework.BundleContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static com.atlassian.jira.plugins.dvcs.scheduler.AciDescriptorRegistrationTask.ACIRegistrationThunk;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AciDescriptorRegistrationTaskThunkTest {
    @Mock
    private BundleContext bundleContext;
    @Mock
    private ServiceAccessor<ACIRegistrationService> serviceAccessor;
    @Mock
    private ACIRegistrationService registrationService;

    private ConnectApplication connectApplication;

    private ACIRegistrationThunk registrationThunk;

    @BeforeMethod
    public void setup() {
        initMocks(this);

        connectApplication = new ConnectApplication("application", "addon", "descriptor", RemoteApplication.BITBUCKET);
        registrationThunk = new AciDescriptorRegistrationTask.ACIRegistrationThunk(serviceAccessor);
    }

    @Test
    public void testServiceUnavailable() {
        when(serviceAccessor.get()).thenReturn(empty());

        final Optional<ConnectApplication> result = registrationThunk.apply();
        assertThat(result.isPresent(), equalTo(false));
    }

    @Test
    public void testReturnsConnectWhenAllAvailable() {
        when(registrationService.register(eq(BITBUCKET_CONNECTOR_APPLICATION_ID), any(String.class),
                eq(RemoteApplication.BITBUCKET))).thenReturn(connectApplication);
        when(serviceAccessor.get()).thenReturn(of(registrationService));

        final Optional<ConnectApplication> result = registrationThunk.apply();
        assertThat(result.isPresent(), equalTo(true));
        assertThat(result.get(), equalTo(connectApplication));

        verify(registrationService).register(eq(BITBUCKET_CONNECTOR_APPLICATION_ID),
                any(String.class), eq(RemoteApplication.BITBUCKET));
    }
}
