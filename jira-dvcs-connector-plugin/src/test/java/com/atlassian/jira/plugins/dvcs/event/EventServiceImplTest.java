package com.atlassian.jira.plugins.dvcs.event;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.util.MockitoTestNgListener;
import com.atlassian.jira.plugins.dvcs.util.SameThreadExecutor;
import com.google.common.collect.ImmutableList;
import org.hamcrest.CoreMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNgListener.class)
public class EventServiceImplTest {
    private static final int REPO_1_ID = 100;
    private static final int REPO_2_ID = 200;

    private static final Date now = new Date();
    private static final ContextAwareSyncEvent eventOneRepoOne =
            new DefaultContextAwareSyncEvent(REPO_1_ID, true, new TestEvent(now, "event one data"));
    private static final ContextAwareSyncEvent eventTwoRepoOne =
            new DefaultContextAwareSyncEvent(REPO_2_ID, true, new TestEvent(now, "event two data"));
    private static final ContextAwareSyncEvent eventGoodRepoTwo =
            new DefaultContextAwareSyncEvent(REPO_2_ID, true, new TestEvent());
    private static final ContextAwareSyncEvent eventBadRepoTwo = new BadEvent(REPO_2_ID, new Date());

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private SyncEventDao syncEventDao;

    @Mock
    private EventLimiterFactory eventLimiterFactory;

    @Mock
    private EventLimiter eventLimiter;

    @Mock
    private Repository repo1;

    @Mock
    private Repository repo2;

    private EventServiceImpl eventService;

    @BeforeMethod
    public void setUp() throws Exception {
        eventService = new EventServiceImpl(eventPublisher, syncEventDao, eventLimiterFactory, new SameThreadExecutor());
        when(eventLimiterFactory.create()).thenReturn(eventLimiter);

        when(repo1.getId()).thenReturn(REPO_1_ID);
        when(repo2.getId()).thenReturn(REPO_2_ID);

        doAnswer(new ForeachConsumeAnswer(eventOneRepoOne)).when(syncEventDao).foreachByRepoId(eq(REPO_1_ID), any(Consumer.class));
        doAnswer(new ForeachConsumeAnswer(eventBadRepoTwo, eventGoodRepoTwo)).when(syncEventDao).foreachByRepoId(eq(REPO_2_ID), any(Consumer.class));
    }

    @Test
    public void storeSavesMappingInDao() throws Exception {
        final TestEvent event = new TestEvent(new Date(0), "my-data");
        final boolean scheduled = true;
        eventService.storeEvent(repo1, event, scheduled);

        ArgumentCaptor<ContextAwareSyncEvent> captor = ArgumentCaptor.forClass(ContextAwareSyncEvent.class);
        verify(syncEventDao).save(captor.capture());

        ContextAwareSyncEvent savedEvent = captor.getValue();
        assertThat(savedEvent, notNullValue());
        assertThat(savedEvent.getRepoId(), equalTo(repo1.getId()));
        assertThat(savedEvent.getDate(), equalTo(event.getDate()));
        assertThat(savedEvent.getEventName(), equalTo(event.getClass().getName()));
        assertThat(savedEvent.asJson(), equalTo("{\"date\":0,\"data\":\"my-data\"}"));
        assertThat(savedEvent.scheduledSync(), equalTo(scheduled));
    }

    @Test
    public void dispatchShouldReadMappingsFromDaoAndSendToEventPublisher() throws Exception {
        eventService.dispatchEvents(repo1);

        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Object.class);
        verify(eventPublisher).publish(captor.capture());

        Object published = captor.getValue();
        assertThat(published, instanceOf(TestEvent.class));
        assertThat(published, equalTo(eventOneRepoOne.getSyncEvent()));
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void dispatchShouldDeleteMappingOnceItHasBeenPublished() throws Exception {
        eventService.dispatchEvents(repo2);

        // both events should be deleted
        ArgumentCaptor<ContextAwareSyncEvent> captor = ArgumentCaptor.forClass(ContextAwareSyncEvent.class);
        verify(syncEventDao, times(2)).delete(captor.capture());

        List<ContextAwareSyncEvent> deletedMappings = captor.getAllValues();
        assertThat(deletedMappings.get(0), is(eventBadRepoTwo));
        assertThat(deletedMappings.get(1), is(eventGoodRepoTwo));
    }

    @Test
    public void dispatchShouldTakeEventLimitsIntoAccount() throws Exception {
        // let the first event through and limit the 2nd
        doAnswer(new ForeachConsumeAnswer(eventOneRepoOne, eventTwoRepoOne))
                .when(syncEventDao).foreachByRepoId(eq(REPO_1_ID), any(Consumer.class));


        when(eventLimiter.isLimitExceeded(any(SyncEvent.class), anyBoolean())).thenReturn(false, true);
        when(eventLimiter.getLimitExceededCount()).thenReturn(1);

        eventService.dispatchEvents(repo1);

        ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Object.class);
        verify(eventPublisher, times(2)).publish(captor.capture());

        List<Object> published = captor.getAllValues();
        assertThat(published.size(), equalTo(2));
        assertThat("first event should be published", published.get(0), instanceOf(TestEvent.class));
        assertThat("limit exceeded event should be raised", published.get(1), instanceOf(LimitExceededEvent.class));
        assertThat("event should contain dropped event count", ((LimitExceededEvent) published.get(1)).getDroppedEventCount(), equalTo(1));
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void destroyShouldShutdownExecutor() throws Exception {
        BlockingQueue<Runnable> queue = mock(BlockingQueue.class);
        ThreadPoolExecutor executor = mock(ThreadPoolExecutor.class);
        when(executor.getQueue()).thenReturn(queue);

        EventServiceImpl eventService = new EventServiceImpl(eventPublisher, syncEventDao, eventLimiterFactory, executor);
        eventService.destroy();

        verify(executor).shutdown();
        verify(executor, never()).shutdownNow();
        verify(queue).clear();
        verify(executor).awaitTermination(anyLong(), any(TimeUnit.class));
    }

    @Test
    public void storeEventCallsDaoCorrectlySingleArg() {
        eventService.storeEvent(eventOneRepoOne);

        verify(syncEventDao).save(eventOneRepoOne);
    }

    @Test
    public void storeEventCallsDaoCorrectlyThreeArgId() {
        eventService.storeEvent(eventTwoRepoOne.getRepoId(), eventTwoRepoOne.getSyncEvent(), eventTwoRepoOne.scheduledSync());
        ArgumentCaptor<ContextAwareSyncEvent> eventCaptor = ArgumentCaptor.forClass(ContextAwareSyncEvent.class);

        verify(syncEventDao).save(eventCaptor.capture());
        ContextAwareSyncEvent capturedEvent = eventCaptor.getValue();
        assertThat(capturedEvent.getRepoId(), is(CoreMatchers.equalTo(eventTwoRepoOne.getRepoId())));
        assertThat(capturedEvent.getSyncEvent(), is(CoreMatchers.equalTo(eventTwoRepoOne.getSyncEvent())));
        assertThat(capturedEvent.scheduledSync(), is(CoreMatchers.equalTo(eventTwoRepoOne.scheduledSync())));
    }

    @Test
    public void storeEventCallsDaoCorrectlyThreeArgRepo() {
        eventService.storeEvent(repo1, eventTwoRepoOne.getSyncEvent(), true);
        ArgumentCaptor<ContextAwareSyncEvent> eventCaptor = ArgumentCaptor.forClass(ContextAwareSyncEvent.class);

        verify(syncEventDao).save(eventCaptor.capture());
        ContextAwareSyncEvent capturedEvent = eventCaptor.getValue();
        assertThat(capturedEvent.getRepoId(), is(CoreMatchers.equalTo(repo1.getId())));
        assertThat(capturedEvent.getSyncEvent(), is(CoreMatchers.equalTo(eventTwoRepoOne.getSyncEvent())));
        assertThat(capturedEvent.scheduledSync(), is(CoreMatchers.equalTo(true)));
    }

    /**
     * Answer for streaming mappings.
     */
    private static class ForeachConsumeAnswer implements Answer<Void> {
        private final ImmutableList<ContextAwareSyncEvent> events;

        public ForeachConsumeAnswer(ContextAwareSyncEvent... syncEvents) {
            this.events = ImmutableList.copyOf(syncEvents);
        }

        @Override
        public Void answer(final InvocationOnMock invocation) throws Throwable {
            final Consumer<ContextAwareSyncEvent> consumer = (Consumer<ContextAwareSyncEvent>) invocation.getArguments()[1];

            events.stream().forEach(consumer);
            return null;
        }
    }
}
