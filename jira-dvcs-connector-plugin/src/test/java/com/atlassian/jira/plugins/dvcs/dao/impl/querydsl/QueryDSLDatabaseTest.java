package com.atlassian.jira.plugins.dvcs.dao.impl.querydsl;

import com.atlassian.activeobjects.ao.AtlassianTablePrefix;
import com.atlassian.activeobjects.ao.PrefixedSchemaConfiguration;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.internal.Prefix;
import com.atlassian.activeobjects.internal.SimplePrefix;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.BranchAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.ChangesetAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.OrganizationAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.PullRequestAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.RepositoryAOPopulator;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.BranchMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.ChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToBranchMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationToProjectMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryToChangesetMapping;
import com.atlassian.jira.plugins.dvcs.activity.PullRequestParticipantMapping;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestIssueKeyMapping;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestMapping;
import com.atlassian.jira.plugins.dvcs.dao.impl.BranchDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.ChangesetDaoImpl;
import com.atlassian.jira.plugins.dvcs.dao.impl.QueryDslFeatureHelper;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.pocketknife.api.querydsl.schema.DialectProvider;
import com.atlassian.pocketknife.api.querydsl.schema.SchemaProvider;
import com.atlassian.pocketknife.internal.querydsl.schema.DefaultSchemaProvider;
import com.atlassian.pocketknife.spi.querydsl.DefaultDialectConfiguration;
import com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessor;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import net.java.ao.DatabaseProvider;
import net.java.ao.DisposableDataSource;
import net.java.ao.EntityManager;
import net.java.ao.EntityManagerConfiguration;
import net.java.ao.SchemaConfiguration;
import net.java.ao.atlassian.AtlassianFieldNameConverter;
import net.java.ao.atlassian.AtlassianIndexNameConverter;
import net.java.ao.atlassian.AtlassianSequenceNameConverter;
import net.java.ao.atlassian.AtlassianTableNameConverter;
import net.java.ao.atlassian.AtlassianTriggerNameConverter;
import net.java.ao.atlassian.AtlassianUniqueNameConverter;
import net.java.ao.builder.SimpleNameConverters;
import net.java.ao.db.H2DatabaseProvider;
import net.java.ao.db.MySQLDatabaseProvider;
import net.java.ao.db.OracleDatabaseProvider;
import net.java.ao.db.PostgreSQLDatabaseProvider;
import net.java.ao.db.SQLServerDatabaseProvider;
import net.java.ao.schema.info.CachingEntityInfoResolverFactory;
import net.java.ao.schema.info.EntityInfoResolverFactory;
import org.junit.After;
import org.junit.Before;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator.BITBUCKET;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.dropTable;
import static com.atlassian.pocketknife.test.util.querydsl.StandaloneDatabaseAccessorUtils.setUpDatabaseUsingSQLFile;
import static com.google.common.collect.Collections2.transform;
import static java.util.Objects.requireNonNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Base test for QueryDSLDatabase tests, sets up the database accessor as well as data populators
 * that can be used to set up test data.
 */
public abstract class QueryDSLDatabaseTest {

    protected static final String ISSUE_KEY = "QDSL-1";
    protected static final ArrayList<String> ISSUE_KEYS = Lists.newArrayList(ISSUE_KEY);
    private static final Map<DialectProvider.SupportedDatabase, Class<? extends DatabaseProvider>> dialects = ImmutableMap.of(
            DialectProvider.SupportedDatabase.H2, H2DatabaseProvider.class,
            DialectProvider.SupportedDatabase.SQLSERVER, SQLServerDatabaseProvider.class,
            DialectProvider.SupportedDatabase.MYSQL, MySQLDatabaseProvider.class,
            DialectProvider.SupportedDatabase.ORACLE, OracleDatabaseProvider.class,
            DialectProvider.SupportedDatabase.POSTGRESSQL, PostgreSQLDatabaseProvider.class
    );
    protected ActiveObjects ao;
    protected ChangesetAOPopulator changesetAOPopulator;
    protected RepositoryAOPopulator repositoryAOPopulator;
    protected OrganizationAOPopulator organizationAOPopulator;
    protected PullRequestAOPopulator pullRequestAOPopulator;
    protected BranchAOPopulator branchAOPopulator;

    protected ChangesetDaoQueryDsl changesetDaoQueryDsl;
    protected PullRequestDaoQueryDsl pullRequestDaoQueryDsl;
    protected BranchDaoQueryDsl branchDaoQueryDsl;
    protected OrganizationQueryDslFacade organizationQueryDslFacade;

    protected RepositoryMapping enabledRepository;
    protected OrganizationMapping bitbucketOrganization;

    protected ChangesetMapping changesetMappingWithIssue;
    protected RepositoryPullRequestMapping pullRequestMappingWithIssue;
    protected PullRequestParticipantMapping pullRequestParticipant;
    protected BranchMapping branchMappingWithIssue;
    protected ChangesetDaoImpl changesetDao;
    protected QueryDslFeatureHelper queryDslFeatureHelper;
    protected BranchDaoImpl branchDao;
    protected EntityManager entityManager;
    private String AO_TABLE_NAME_PREFIX = "AO_E8B6CC";
    private StandaloneDatabaseAccessor databaseAccessor;

    @Before
    public void setup() throws Exception {
        final InputStream inputStream = MessageQueueItemDaoQueryDslTest.class.getResourceAsStream("/com.atlassian.jira.plugins.dvcs.dao.impl.querydsl/QueryDSLDatabaseTest.sql");
        databaseAccessor = setUpDatabaseUsingSQLFile(AO_TABLE_NAME_PREFIX, inputStream,
                ChangesetMapping.class,
                RepositoryToChangesetMapping.class,
                RepositoryMapping.class,
                OrganizationMapping.class,
                IssueToChangesetMapping.class,
                RepositoryPullRequestMapping.class,
                PullRequestParticipantMapping.class,
                RepositoryPullRequestIssueKeyMapping.class,
                BranchMapping.class,
                IssueToBranchMapping.class,
                OrganizationToProjectMapping.class);

        entityManager = createEntityManager();

        changesetAOPopulator = new ChangesetAOPopulator(entityManager);
        repositoryAOPopulator = new RepositoryAOPopulator(entityManager);
        organizationAOPopulator = new OrganizationAOPopulator(entityManager);
        pullRequestAOPopulator = new PullRequestAOPopulator(entityManager);
        branchAOPopulator = new BranchAOPopulator(entityManager);
        final SchemaProvider schemaProvider = new DefaultSchemaProvider();

        queryDslFeatureHelper = mock(QueryDslFeatureHelper.class);
        when(queryDslFeatureHelper.isRetrievalUsingQueryDslDisabled()).thenReturn(false);

        organizationQueryDslFacade = new OrganizationQueryDslFacade(databaseAccessor);
        changesetDao = mock(ChangesetDaoImpl.class);
        changesetDaoQueryDsl = new ChangesetDaoQueryDsl(databaseAccessor, changesetDao, queryDslFeatureHelper);
        pullRequestDaoQueryDsl = new PullRequestDaoQueryDsl(databaseAccessor);
        branchDao = mock(BranchDaoImpl.class);
        branchDaoQueryDsl = new BranchDaoQueryDsl(branchDao, databaseAccessor, queryDslFeatureHelper);

        //populate test data
        bitbucketOrganization = organizationAOPopulator.create(BITBUCKET);
        enabledRepository = repositoryAOPopulator.createEnabledRepository(bitbucketOrganization);
        changesetMappingWithIssue = changesetAOPopulator.createCSM(changesetAOPopulator.getDefaultCSParams(), ISSUE_KEY, enabledRepository);
        pullRequestMappingWithIssue = pullRequestAOPopulator.createPR("PR FOR" + ISSUE_KEY, ISSUE_KEY, enabledRepository);
        pullRequestParticipant = pullRequestAOPopulator.createParticipant("hoo", false, "reviewer", pullRequestMappingWithIssue);
        pullRequestMappingWithIssue = entityManager.get(RepositoryPullRequestMapping.class, pullRequestMappingWithIssue.getID());
        branchMappingWithIssue = branchAOPopulator.createBranch("something branch", ISSUE_KEY, enabledRepository);

        //prime the schema so it is ready for access
        schemaProvider.prime(databaseAccessor.connection());
        ao = new TestActiveObjects(entityManager);
    }

    @After
    public void cleanUp() throws SQLException {
        dropTable("AO_E8B6CC_REPO_TO_CHANGESET", databaseAccessor);
        dropTable("AO_E8B6CC_REPOSITORY_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ORGANIZATION_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ISSUE_TO_CHANGESET", databaseAccessor);
        dropTable("AO_E8B6CC_PULL_REQUEST", databaseAccessor);
        dropTable("AO_E8B6CC_PR_PARTICIPANT", databaseAccessor);
        dropTable("AO_E8B6CC_PR_ISSUE_KEY", databaseAccessor);
        dropTable("AO_E8B6CC_BRANCH", databaseAccessor);
        dropTable("AO_E8B6CC_ISSUE_TO_BRANCH", databaseAccessor);
        dropTable("AO_E8B6CC_CHANGESET_MAPPING", databaseAccessor);
        dropTable("AO_E8B6CC_ORG_TO_PROJECT", databaseAccessor);
    }

    //stolen from StandaloneAoDatabaseAccessor as a temporary measure
    private EntityManager createEntityManager() throws NoSuchMethodException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final DialectProvider.SupportedDatabase supportedDatabase = databaseAccessor.doWithConnection(connection ->
                new DefaultDialectConfiguration(new DefaultSchemaProvider())
                        .getDialectConfig(connection)
                        .getDatabaseInfo()
                        .getSupportedDatabase());

        DatabaseProvider databaseProvider = dialects.get(supportedDatabase)
                .getConstructor(DisposableDataSource.class, String.class)
                .newInstance(new MinimalDisposableDataSource(), databaseAccessor.getSchema());

        return new EntityManager(databaseProvider, new AtlassianEntityManagerConfiguration(AO_TABLE_NAME_PREFIX));

    }

    protected final Collection<Integer> extractIds(final Collection<Changeset> changeSets) {
        return transform(changeSets, Changeset::getId);
    }

    /**
     * Implementation that closely matches the ao-plugin configuration.
     */
    private class AtlassianEntityManagerConfiguration implements EntityManagerConfiguration {

        private final Prefix prefix;

        public AtlassianEntityManagerConfiguration(@Nonnull final String prefix) {
            this.prefix = new SimplePrefix(requireNonNull(prefix));
        }

        @Override
        public boolean useWeakCache() {
            return false;
        }

        @Override
        public net.java.ao.schema.NameConverters getNameConverters() {
            return new SimpleNameConverters(
                    new AtlassianTableNameConverter(new AtlassianTablePrefix(prefix)),
                    new AtlassianFieldNameConverter(),
                    new AtlassianSequenceNameConverter(),
                    new AtlassianTriggerNameConverter(),
                    new AtlassianIndexNameConverter(),
                    new AtlassianUniqueNameConverter()
            );
        }

        @Override
        public SchemaConfiguration getSchemaConfiguration() {
            return new PrefixedSchemaConfiguration(prefix);
        }

        @Override
        public EntityInfoResolverFactory getEntityInfoResolverFactory() {
            return new CachingEntityInfoResolverFactory();
        }
    }

    /**
     * AO compatible data source that provides just a connection and throws an exception on every other operation.
     */
    private class MinimalDisposableDataSource implements DisposableDataSource {

        @Override
        public Connection getConnection() throws SQLException {
            return databaseAccessor.connection();
        }

        @Override
        public Connection getConnection(String username, String password) throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public PrintWriter getLogWriter() throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setLogWriter(PrintWriter out) throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public int getLoginTimeout() throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setLoginTimeout(int seconds) throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void dispose() {
            throw new UnsupportedOperationException();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            throw new UnsupportedOperationException();
        }
    }


}
