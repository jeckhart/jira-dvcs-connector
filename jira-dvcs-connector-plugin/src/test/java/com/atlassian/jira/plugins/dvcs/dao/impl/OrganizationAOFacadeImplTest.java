package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.crypto.Encryptor;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.entry;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrganizationAOFacadeImplTest {
    private static final String ADMIN_USERNAME = "adminUserName";
    private static final String ADMIN_PASSWORD = "password";
    private static final String OAUTH_KEY = "oauthKey";
    private static final String OAUTH_SECRET = "oauthSecret";
    private static final String PRINCIPAL_ID = "principalId";
    private static final String DVCS_TYPE = "bitbucket";
    private static final String NAME = "organizationName";
    private static final String HOST_URL = "organizationHostUrl";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String APPROVED_STATE = "APPROVED";
    private static final boolean AUTO_LINK = true;
    private static final boolean SMART_COMMITS = false;

    private static final Credential UNAUTHENTICATED_CREDENTIAL = CredentialFactory.createUnauthenticatedCredential();
    private static final Credential BASIC_CREDENTIAL = CredentialFactory.createBasicAuthCredential(ADMIN_USERNAME, ADMIN_PASSWORD);
    private static final Credential OAUTH_2LO_CREDENTIAL = CredentialFactory.create2LOCredential(OAUTH_KEY, OAUTH_SECRET);
    private static final Credential OAUTH_3LO_CREDENTIAL = CredentialFactory.create3LOCredential(OAUTH_KEY, OAUTH_SECRET, ACCESS_TOKEN);
    private static final Credential PRINCIPAL_ID_CREDENTIAL = CredentialFactory.createPrincipalCredential(PRINCIPAL_ID);

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ActiveObjects activeObjects;

    @Mock
    private OrganizationMapping organizationMapping;

    @Mock
    private Encryptor encryptor;

    @Captor
    private ArgumentCaptor<Map<String, Object>> mapArgumentCaptor;

    private OrganizationAOFacadeImpl classUnderTest;

    @Before
    public void setup() {
        classUnderTest = new OrganizationAOFacadeImpl(activeObjects, encryptor);

        when(organizationMapping.getApprovalState()).thenReturn(Organization.ApprovalState.APPROVED.name());
        when(activeObjects.executeInTransaction(isA(TransactionCallback.class))).thenAnswer(
                invocationOnMock -> ((TransactionCallback) invocationOnMock.getArguments()[0]).doInTransaction());
        when(activeObjects.get(eq(OrganizationMapping.class), anyInt())).thenReturn(organizationMapping);
        when(activeObjects.create(eq(OrganizationMapping.class), isA(Map.class))).thenReturn(organizationMapping);
        when(activeObjects.find(eq(OrganizationMapping.class), anyString(), any(Object.class))).thenReturn(
                new OrganizationMapping[]{organizationMapping});
        when(encryptor.encrypt(any(String.class), any(String.class), any(String.class))).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void testSaveNewOrganizationWithBasicCredentials() {
        classUnderTest.save(createSampleOrganization(BASIC_CREDENTIAL));

        verify(activeObjects).create(eq(OrganizationMapping.class), mapArgumentCaptor.capture());

        assertThat(mapArgumentCaptor.getValue()).contains(
                entry(OrganizationMapping.DVCS_TYPE, DVCS_TYPE),
                entry(OrganizationMapping.HOST_URL, HOST_URL),
                entry(OrganizationMapping.NAME, NAME),
                entry(OrganizationMapping.AUTOLINK_NEW_REPOS, AUTO_LINK),
                entry(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, SMART_COMMITS),
                entry(OrganizationMapping.ADMIN_USERNAME, ADMIN_USERNAME),
                entry(OrganizationMapping.ADMIN_PASSWORD, ADMIN_PASSWORD),
                entry(OrganizationMapping.APPROVAL_STATE, APPROVED_STATE));
    }

    @Test
    public void testSaveNewOrganizationWith2LOCredentials() {
        classUnderTest.save(createSampleOrganization(OAUTH_2LO_CREDENTIAL));

        verify(activeObjects).create(eq(OrganizationMapping.class), mapArgumentCaptor.capture());

        assertThat(mapArgumentCaptor.getValue()).contains(
                entry(OrganizationMapping.DVCS_TYPE, DVCS_TYPE),
                entry(OrganizationMapping.HOST_URL, HOST_URL),
                entry(OrganizationMapping.NAME, NAME),
                entry(OrganizationMapping.AUTOLINK_NEW_REPOS, AUTO_LINK),
                entry(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, SMART_COMMITS),
                entry(OrganizationMapping.OAUTH_KEY, OAUTH_KEY),
                entry(OrganizationMapping.OAUTH_SECRET, OAUTH_SECRET),
                entry(OrganizationMapping.APPROVAL_STATE, APPROVED_STATE));
    }

    @Test
    public void testSaveNewOrganizationWith3LOCredentials() {
        classUnderTest.save(createSampleOrganization(OAUTH_3LO_CREDENTIAL));

        verify(activeObjects).create(eq(OrganizationMapping.class), mapArgumentCaptor.capture());

        assertThat(mapArgumentCaptor.getValue()).contains(
                entry(OrganizationMapping.DVCS_TYPE, DVCS_TYPE),
                entry(OrganizationMapping.HOST_URL, HOST_URL),
                entry(OrganizationMapping.NAME, NAME),
                entry(OrganizationMapping.AUTOLINK_NEW_REPOS, AUTO_LINK),
                entry(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, SMART_COMMITS),
                entry(OrganizationMapping.OAUTH_KEY, OAUTH_KEY),
                entry(OrganizationMapping.OAUTH_SECRET, OAUTH_SECRET),
                entry(OrganizationMapping.ACCESS_TOKEN, ACCESS_TOKEN),
                entry(OrganizationMapping.APPROVAL_STATE, APPROVED_STATE));
    }

    @Test
    public void testSaveNewOrganizationWithUnauthenticatedCredentials() {
        classUnderTest.save(createSampleOrganization(UNAUTHENTICATED_CREDENTIAL));

        verify(activeObjects).create(eq(OrganizationMapping.class), mapArgumentCaptor.capture());

        assertThat(mapArgumentCaptor.getValue()).contains(
                entry(OrganizationMapping.DVCS_TYPE, DVCS_TYPE),
                entry(OrganizationMapping.HOST_URL, HOST_URL),
                entry(OrganizationMapping.NAME, NAME),
                entry(OrganizationMapping.AUTOLINK_NEW_REPOS, AUTO_LINK),
                entry(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, SMART_COMMITS),
                entry(OrganizationMapping.ADMIN_USERNAME, null),
                entry(OrganizationMapping.ADMIN_PASSWORD, null),
                entry(OrganizationMapping.OAUTH_KEY, null),
                entry(OrganizationMapping.OAUTH_SECRET, null),
                entry(OrganizationMapping.ACCESS_TOKEN, null),
                entry(OrganizationMapping.PRINCIPAL_ID, null),
                entry(OrganizationMapping.APPROVAL_STATE, Organization.ApprovalState.APPROVED.name()));
    }

    @Test
    public void testSaveNewOrganizationWithPrincipalCredentials() {
        classUnderTest.save(createSampleOrganization(PRINCIPAL_ID_CREDENTIAL));

        verify(activeObjects).create(eq(OrganizationMapping.class), mapArgumentCaptor.capture());

        assertThat(mapArgumentCaptor.getValue()).contains(
                entry(OrganizationMapping.DVCS_TYPE, DVCS_TYPE),
                entry(OrganizationMapping.HOST_URL, HOST_URL),
                entry(OrganizationMapping.NAME, NAME),
                entry(OrganizationMapping.AUTOLINK_NEW_REPOS, AUTO_LINK),
                entry(OrganizationMapping.SMARTCOMMITS_FOR_NEW_REPOS, SMART_COMMITS),
                entry(OrganizationMapping.PRINCIPAL_ID, PRINCIPAL_ID),
                entry(OrganizationMapping.APPROVAL_STATE, APPROVED_STATE));
    }

    @Test
    public void testUpdateExistingOrganization() {
        final int existingOrgId = 123;

        Organization organization = createSampleOrganization(BASIC_CREDENTIAL);
        organization.setId(existingOrgId);

        classUnderTest.save(organization);

        verify(organizationMapping).setDvcsType(eq(DVCS_TYPE));
        verify(organizationMapping).setHostUrl(eq(HOST_URL));
        verify(organizationMapping).setName(eq(NAME));
        verify(organizationMapping).setAutolinkNewRepos(eq(AUTO_LINK));
        verify(organizationMapping).setAdminPassword(eq(ADMIN_PASSWORD));
        verify(organizationMapping).setAdminUsername(eq(ADMIN_USERNAME));
        verify(organizationMapping).setApprovalState(Organization.ApprovalState.APPROVED.name());
        verify(organizationMapping).save();
    }

    @Test
    public void testDeserializeDefaultGroups() {
        assertThat(classUnderTest.deserializeDefaultGroups(null)).isEmpty();
        assertThat(classUnderTest.deserializeDefaultGroups(" ")).isEmpty();
        assertThat(classUnderTest.deserializeDefaultGroups(";")).isEmpty();
        assertThat(classUnderTest.deserializeDefaultGroups(" ;")).isEmpty();

        assertThat(classUnderTest.deserializeDefaultGroups("a;b")).containsOnly(new Group("a"), new Group("b"));

        assertThat(classUnderTest.deserializeDefaultGroups("abraka dab;raka")).containsOnly(new Group("abraka dab"), new Group("raka"));
    }

    @Test
    public void testWillSerializeDefaultGroupsWithSimpleNames() {
        String serializedGroups = classUnderTest.serializeDefaultGroups(Sets.newHashSet(new Group("a"), new Group("b")));
        List<String> expectedResults = Lists.newArrayList("a", "b");
        List<String> actualResults = Lists.newArrayList(Splitter.on(";").split(serializedGroups));
        assertThat(expectedResults).containsAll(actualResults);
        assertThat(expectedResults).hasSameSizeAs(actualResults);
    }

    @Test
    public void testWillSerializeDefaultGroupsWithComplexNames() {
        String serializedGroups = classUnderTest.serializeDefaultGroups(Sets.newHashSet(new Group("abraka dab"), new Group("raka")));
        List<String> expectedResults = Lists.newArrayList("raka", "abraka dab");
        List<String> actualResults = Lists.newArrayList(Splitter.on(";").split(serializedGroups));
        assertThat(expectedResults).containsAll(actualResults);
        assertThat(expectedResults).hasSameSizeAs(actualResults);
    }

    private Organization createSampleOrganization(Credential credential) {
        Organization organization = new Organization();
        organization.setId(0);
        organization.setHostUrl(HOST_URL);
        organization.setName(NAME);
        organization.setDvcsType(DVCS_TYPE);
        organization.setAutolinkNewRepos(AUTO_LINK);
        organization.setSmartcommitsOnNewRepos(SMART_COMMITS);
        organization.setCredential(credential);
        organization.setApprovalState(Organization.ApprovalState.APPROVED);
        return organization;
    }
}
