package com.atlassian.jira.plugins.dvcs.ondemand;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class AccountsConfigLifecyclerTest {
    private AccountsConfigLifecycler lifecycler;

    @Mock
    private AccountsConfigService accountsConfigService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        lifecycler = new AccountsConfigLifecycler(accountsConfigService);
    }

    @Test
    public void shouldScheduleReloadUponLifecycleStart() {
        // Invoke
        lifecycler.onStart();

        // Check
        verify(accountsConfigService).scheduleReload();
    }
}
