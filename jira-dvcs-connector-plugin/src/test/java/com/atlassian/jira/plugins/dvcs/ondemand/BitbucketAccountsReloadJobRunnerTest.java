package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.atlassian.scheduler.JobRunnerResponse.success;
import static java.util.Collections.singletonMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BitbucketAccountsReloadJobRunnerTest {
    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private BitbucketAccountsConfigService bitbucketAccountsConfigService;

    @Mock
    private JobRunnerRequest jobRunnerRequest;

    private BitbucketAccountsReloadJobRunner jobRunner;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(applicationContext.getBeansOfType(BitbucketAccountsConfigService.class))
                .thenReturn(singletonMap("ignored", bitbucketAccountsConfigService));
        this.jobRunner = new BitbucketAccountsReloadJobRunner();
        this.jobRunner.setApplicationContext(applicationContext);
    }

    @Test
    public void runningTheJobShouldCauseTheServiceToReload() {
        // Invoke
        final JobRunnerResponse response = jobRunner.runJob(jobRunnerRequest);

        // Check
        assertThat(response, is(success()));
        verify(bitbucketAccountsConfigService).reload();
    }
}
