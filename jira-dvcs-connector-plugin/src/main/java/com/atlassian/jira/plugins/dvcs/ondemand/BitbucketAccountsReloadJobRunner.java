package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

import static com.atlassian.scheduler.JobRunnerResponse.success;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Runs the scheduled job of reloading Bitbucket accounts.
 */
@Component
public class BitbucketAccountsReloadJobRunner implements ApplicationContextAware, JobRunner {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) {
        this.applicationContext = checkNotNull(applicationContext);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(@Nonnull final JobRunnerRequest request) {
        getConfigService().reload();
        return success();
    }

    private BitbucketAccountsConfigService getConfigService() {
        // Not injecting this via DI because it gives intermittent circular dependencies
        @SuppressWarnings("unchecked")
        final Collection<BitbucketAccountsConfigService> services =
                applicationContext.getBeansOfType(BitbucketAccountsConfigService.class).values();
        checkArgument(services.size() == 1, "Expected one service but found %d: %s", services.size(), services);
        return services.iterator().next();
    }
}
