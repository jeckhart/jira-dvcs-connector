package com.atlassian.jira.plugins.dvcs.rest.json;

/**
 * Used as JSON DTO representing raw Organization Credential (of any Credential type).
 */
public class CredentialJson {
    private String username;
    private String password;
    private String key;
    private String secret;
    private String token;
    private String principalId;

    public CredentialJson() {

    }

    public String getUsername() {
        return username;
    }

    public CredentialJson setUsername(final String value) {
        this.username = value;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CredentialJson setPassword(final String value) {
        this.password = value;
        return this;
    }

    public String getKey() {
        return key;
    }

    public CredentialJson setKey(final String value) {
        this.key = value;
        return this;
    }

    public String getSecret() {
        return secret;
    }

    public CredentialJson setSecret(final String value) {
        this.secret = value;
        return this;
    }

    public String getToken() {
        return token;
    }

    public CredentialJson setToken(final String value) {
        this.token = value;
        return this;
    }

    public String getPrincipalId() {
        return principalId;
    }

    public CredentialJson setPrincipalId(final String value) {
        this.principalId = value;
        return this;
    }
}
