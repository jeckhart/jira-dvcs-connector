package com.atlassian.jira.plugins.dvcs.rest;

import com.atlassian.jira.exception.NotFoundException;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import org.slf4j.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {
    private static final Logger log = ExceptionLogger.getLogger(ExceptionHandler.class);

    @Override
    public Response toResponse(final Exception e) {
        if (e instanceof NotFoundException) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity(e.getMessage())
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }
        if (e instanceof IllegalArgumentException) {
            log.debug("Responding to request with 400", e);
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .type(MediaType.TEXT_PLAIN)
                    .build();
        }
        if (e instanceof WebApplicationException) {
            log.debug("Responding to request with WebApplicationException", e);
            return ((WebApplicationException) e).getResponse();
        }

        log.warn("Uncaught exception in REST layer: " + e, e);
        return Response.serverError().build();
    }
}
