package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.model.credential.OAuthCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.OAuthCredentialVisitor;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfig.BitbucketAccountInfo;
import com.atlassian.jira.plugins.dvcs.ondemand.AccountsConfig.Links;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.BitbucketDetails;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.Executors.newFixedThreadPool;

@ExportAsService(AccountsConfigService.class)
@Component
public class BitbucketAccountsConfigService implements AccountsConfigService, DisposableBean // TODO move to BB module
{
    private static final Logger log = LoggerFactory.getLogger(BitbucketAccountsConfigService.class);

    private static final String THREAD_PREFIX = "DVCSConnector.BitbucketAccountsConfigService";

    private final AccountsConfigProvider configProvider;
    private final BitbucketAccountsReloadJobScheduler bitbucketAccountsReloadJob;
    private final ExecutorService executorService;
    private final OrganizationService organizationService;

    @Autowired
    public BitbucketAccountsConfigService(
            final AccountsConfigProvider configProvider,
            final OrganizationService organizationService,
            final BitbucketAccountsReloadJobScheduler bitbucketAccountsReloadJob) {
        this.bitbucketAccountsReloadJob = checkNotNull(bitbucketAccountsReloadJob);
        this.configProvider = checkNotNull(configProvider);
        this.executorService = newFixedThreadPool(1, namedThreadFactory(THREAD_PREFIX));
        this.organizationService = checkNotNull(organizationService);
    }

    private static void assertNotBlank(String string, String msg) {
        if (isBlank(string)) {
            throw new IllegalArgumentException(msg);
        }
    }

    private static boolean isBlank(String string) {
        return StringUtils.isBlank(string);
    }

    @Override
    public void destroy() throws Exception {
        executorService.shutdown();
        if (!executorService.awaitTermination(1, TimeUnit.MINUTES)) {
            log.error("Unable properly shutdown queued tasks.");
        }
    }

    @Override
    public void scheduleReload() {
        if (!supportsIntegratedAccounts()) {
            return;
        }

        bitbucketAccountsReloadJob.schedule();
    }

    @Override
    public void reloadAsync() {
        if (!supportsIntegratedAccounts()) {
            return;
        }

        executorService.submit(this::reloadInternal);
    }

    @Override
    public void reload() {
        if (!supportsIntegratedAccounts()) {
            return;
        }

        reloadInternal();
    }

    private void reloadInternal() {
        log.info("Reloading integrated account");
        //
        AccountsConfig configuration = configProvider.provideConfiguration();
        Organization existingAccount = organizationService.findIntegratedAccount();
        //
        // new or update
        //

        if (existingAccount == null) {
            if (configuration != null) {
                if (hasIntegratedAccount(configuration)) {
                    doNewAccount(configuration);
                } else {
                    log.debug("No integrated account found in provided configuration.");
                }
            } else {
                // probably not ondemand instance
                log.debug("No integrated account found and no configuration is provided.");
            }
        } else {
            // integrated account found
            if (configuration != null) {
                doUpdateConfiguration(configuration, existingAccount);
            } else {
                log.info("Integrated account has been found and no configuration is provided. Deleting integrated account.");
                removeAccount(existingAccount);
            }
        }
    }

    private void doNewAccount(final AccountsConfig configuration) {
        final AccountInfo info = toInfoNewAccount(configuration);
        final Organization userAddedAccount = getUserAddedAccount(info);

        if (userAddedAccount == null) {
            // create brand new
            log.info("Creating new integrated account.");
            final Organization newOrganization = createNewOrganization(info);
            organizationService.save(newOrganization);
            log.info("Created new integrated account.");
        } else {
            log.info("Found the same user-added account.");
            markAsIntegratedAccount(userAddedAccount, info);
            log.info("Marked the user-added account as integrated account");
        }
    }

    private void markAsIntegratedAccount(Organization userAddedAccount, AccountInfo info) {
        organizationService.updateCredentials(userAddedAccount.getId(), CredentialFactory.create2LOCredential(info.oauthKey, info.oauthSecret));
    }

    private Organization getUserAddedAccount(AccountInfo info) {
        final Organization maybeUserAddedAccount = organizationService.getByHostAndName(BitbucketDetails.getHostUrl(), info.accountName);
        return Optional.ofNullable(maybeUserAddedAccount)
                .flatMap(userAddedAccount -> userAddedAccount.getCredential().accept(ThreeLeggedOAuthCredential.visitor()))
                .map(credential -> maybeUserAddedAccount)
                .orElse(null);
    }

    /**
     * BL comes from https://sdog.jira.com/wiki/pages/viewpage.action?pageId=47284285
     */
    private void doUpdateConfiguration(AccountsConfig configuration, Organization existingNotNullAccount) {
        AccountInfo providedConfig = toInfoExistingAccount(configuration);

        if (providedConfig != null) {
            // modify :?
            Organization userAddedAccount = getUserAddedAccount(providedConfig);

            // we have no user-added account with the same name
            if (userAddedAccount == null) {
                if (configHasChanged(existingNotNullAccount, providedConfig)) {
                    log.info("Detected credentials change.");
                    // BBC-513 users probably changed key/secret from UI, ignore
                    // valuse in ondemand.properties files and keep using existing ones
                } else if (accountNameHasChanged(existingNotNullAccount, providedConfig)) {
                    log.info("Detected integrated account name change.");
                    removeAccount(existingNotNullAccount);
                    organizationService.save(createNewOrganization(providedConfig));
                } else {
                    // nothing has changed
                    log.info("No changes detect on integrated account");
                }
            } else {
                // should not happen
                // existing integrated account with the same name as user added
                log.warn("Detected existing integrated account with the same name as user added. Removing both.");
                removeAccount(userAddedAccount);
                // as provided config is null, remove also integrated account
                removeAccount(existingNotNullAccount);
            }
        } else {
            // delete account
            removeAccount(existingNotNullAccount);
        }
    }

    /**
     * @param configuration the configuration to check
     * @return True if there is an integrated account
     */
    private boolean hasIntegratedAccount(final AccountsConfig configuration) {
        return configuration.getSysadminApplicationLinks().stream().findFirst()
                .map(Links::getBitbucket)
                .filter(Objects::nonNull)
                .map(accts -> !accts.isEmpty())
                .orElse(false);
    }

    private boolean configHasChanged(Organization existingNotNullAccount, AccountInfo info) {
        final Optional<OAuthCredential> oAuthCredentialOptional =
                existingNotNullAccount.getCredential().accept(OAuthCredentialVisitor.visitor());
        if (oAuthCredentialOptional.isPresent()) {
            final OAuthCredential oAuthCredential = oAuthCredentialOptional.get();
            return StringUtils.equals(info.accountName, existingNotNullAccount.getName()) &&
                    (!StringUtils.equals(info.oauthKey, oAuthCredential.getKey()) ||
                            !StringUtils.equals(info.oauthSecret, oAuthCredential.getSecret()));
        } else {
            return false;
        }
    }

    private boolean accountNameHasChanged(Organization existingNotNullAccount, AccountInfo providedConfig) {
        return !StringUtils.equals(providedConfig.accountName, existingNotNullAccount.getName());
    }

    private void removeAccount(final Organization organization) {
        organizationService.remove(organization.getId());
    }

    private AccountInfo toInfoNewAccount(final AccountsConfig configuration) {
        try {
            AccountInfo info = new AccountInfo();
            //
            // crawl to information
            //
            Links links = configuration.getSysadminApplicationLinks().get(0);
            BitbucketAccountInfo bitbucketAccountInfo = links.getBitbucket().get(0);
            //
            info.accountName = bitbucketAccountInfo.getAccount();
            info.oauthKey = bitbucketAccountInfo.getKey();
            info.oauthSecret = bitbucketAccountInfo.getSecret();
            //
            assertNotBlank(info.accountName, "accountName have to be provided for new account");
            assertNotBlank(info.oauthKey, "oauthKey have to be provided for new account");
            assertNotBlank(info.oauthSecret, "oauthSecret have to be provided for new account");
            //
            return info;

        } catch (Exception e) {
            throw new IllegalStateException("Wrong configuration.", e);
        }
    }

    private AccountInfo toInfoExistingAccount(AccountsConfig configuration) {
        //
        // try to find configuration, otherwise assuming it is deletion of the integrated account
        //
        Links links;
        try {
            links = configuration.getSysadminApplicationLinks().get(0);
        } catch (Exception e) {
            log.debug("Bitbucket links not present. " + e + ": " + e.getMessage());
            return null;
        }

        BitbucketAccountInfo bitbucketAccountInfo = null;

        if (links != null) {
            try {
                bitbucketAccountInfo = links.getBitbucket().get(0);
            } catch (Exception e) {
                log.debug("Bitbucket accounts info not present. " + e + ": " + e.getMessage());
                return null;
            }
        }

        AccountInfo info = new AccountInfo();

        if (bitbucketAccountInfo != null) {
            info.accountName = bitbucketAccountInfo.getAccount();
            info.oauthKey = bitbucketAccountInfo.getKey();
            info.oauthSecret = bitbucketAccountInfo.getSecret();
        }

        if (isBlank(info.accountName)) {
            log.debug("accountName is empty assuming deletion");
            return null;
        }
        if (isBlank(info.oauthKey)) {
            log.debug("oauthKey is empty assuming deletion");
            return null;
        }
        if (isBlank(info.oauthSecret)) {
            log.debug("oauthSecret is empty assuming deletion");
            return null;
        }
        return info;
    }

    private boolean supportsIntegratedAccounts() {
        return configProvider.supportsIntegratedAccounts();
    }

    private Organization createNewOrganization(AccountInfo info) {
        Organization newOrganization = new Organization();
        copyValues(info, newOrganization);
        return newOrganization;
    }

    private Organization copyValues(AccountInfo info, Organization organization) {
        organization.setId(0);
        organization.setName(info.accountName);
        organization.setCredential(CredentialFactory.create2LOCredential(info.oauthKey, info.oauthSecret));
        organization.setHostUrl(BitbucketDetails.getHostUrl());
        organization.setDvcsType("bitbucket");
        organization.setAutolinkNewRepos(true);
        organization.setSmartcommitsOnNewRepos(true);

        return organization;
    }

    private static class AccountInfo {
        String accountName;
        String oauthKey;
        String oauthSecret;
    }
}
