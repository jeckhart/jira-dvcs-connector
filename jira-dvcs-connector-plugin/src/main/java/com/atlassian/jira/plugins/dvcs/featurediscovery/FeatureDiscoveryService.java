package com.atlassian.jira.plugins.dvcs.featurediscovery;

/**
 * A service for querying and setting the "user has seen feature discovery" flag for the current user.
 */
public interface FeatureDiscoveryService {

    /**
     * Return whether the current logged in user has seen the feature discovery for DVCS Connector
     * yet on this instance.
     *
     * @return <code>true</code> if the feature discovery has already been seen by this user;
     * <code>false</code> otherwise.
     */
    boolean hasUserSeenFeatureDiscovery();

    /**
     * Mark the current logged in user as having seen the feature discovery for DVCS Connector.
     *
     * @param hasSeenFeatureDiscovery Whether or not the user has seen the feature discovery tour
     */
    void markUserAsHavingSeenFeatureDiscovery(boolean hasSeenFeatureDiscovery);

}
