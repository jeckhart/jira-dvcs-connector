package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringPath;

public class QIssueToChangesetMapping extends EnhancedRelationalPathBase<QIssueToChangesetMapping> implements IssueKeyedMapping {
    private static final String AO_TABLE_NAME = "AO_E8B6CC_ISSUE_TO_CHANGESET";
    private static final long serialVersionUID = -6856701647687466768L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final NumberPath<Integer> CHANGESET_ID = createInteger("CHANGESET_ID");
    public final StringPath ISSUE_KEY = createString("ISSUE_KEY");
    public final StringPath PROJECT_KEY = createString("PROJECT_KEY");
    public final com.querydsl.sql.PrimaryKey<QIssueToChangesetMapping> ISSUETOCHANGESET_PK = createPrimaryKey(ID);

    public QIssueToChangesetMapping() {
        super(QIssueToChangesetMapping.class, AO_TABLE_NAME);
    }

    @Override
    public SimpleExpression getIssueKeyExpression() {
        return ISSUE_KEY;
    }
}