package com.atlassian.jira.plugins.dvcs.obr.install;

/**
 * This class is here just so we can (OSGi) export a package that can then be imported by the jira-software-application-plugin.
 * This is required for the purpose of OBR installation. A new package is deliberately introduced to minimise the dependencies
 * jira-software-application-plugin has on other plugins.
 */
public class OBRInstall {
}
