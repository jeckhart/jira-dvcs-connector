package com.atlassian.jira.plugins.dvcs.model;

import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.beust.jcommander.internal.Sets;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;

public class OrganizationTest {
    @Test
    public void instancesShouldBeSerializableForUseWithinCacheKeys() {
        // Set up
        final Organization original = new Organization();
        original.setDefaultGroups(singleton(new Group("slug", "name")));
        original.setRepositories(singletonList(new Repository()));

        // Invoke
        final Organization roundTripped = (Organization) deserialize(serialize(original));

        // Check
        assertThat(original, equalTo(roundTripped));
        assertThat(roundTripped, equalTo(original));
        assertThat(original, equalTo(roundTripped));
    }

    @Test
    public void testClonedInstance() {
        final Organization original = new Organization();
        final List<Group> groups = ImmutableList.of(new Group("slug", "group"));
        final Set<Group> defaultGroups = Sets.newHashSet();
        defaultGroups.add(new Group("slug", "default"));

        original.setAutolinkNewRepos(true);
        original.setCredential(CredentialFactory.create3LOCredential("oauthKey", "oauthSecret", "accessToken"));
        original.setDefaultGroups(defaultGroups);
        original.setDvcsType("bitbucket");
        original.setHostUrl("hostUrl");
        original.setId(100);
        original.setName("name");
        original.setGroups(groups);
        original.setOrganizationUrl("url");
        original.setSmartcommitsOnNewRepos(true);

        Organization clonedOrg = new Organization(original);

        assertThat(clonedOrg == original, equalTo(false));
        assertThat(EqualsBuilder.reflectionEquals(clonedOrg, original), equalTo(true));

        // assert transient fields missed by EqualsBuilder.reflectionEquals
        assertThat(clonedOrg.getCredential(), equalTo(original.getCredential()));
        assertThat(clonedOrg.getDefaultGroups().iterator().next(), sameInstance((original.getDefaultGroups().iterator().next())));
        assertThat(clonedOrg.getGroups().iterator().next(), equalTo(original.getGroups().iterator().next()));
    }

    @Test
    public void testIsIntegratedAccountWhenTrue() {
        Organization organization = new Organization();
        organization.setCredential(CredentialFactory.create2LOCredential("hello", "world"));
        assertThat(organization.isIntegratedAccount(), equalTo(true));
    }

    @Test
    public void testIsIntegratedAccountWhenNull() {
        Organization organization = new Organization();
        organization.setCredential(null);
        assertThat(organization.isIntegratedAccount(), equalTo(false));
    }

    @Test
    public void testIsIntegratedAccountWhenNon2LO() {
        Organization organization = new Organization();
        organization.setCredential(CredentialFactory.createPrincipalCredential("hello"));
        assertThat(organization.isIntegratedAccount(), equalTo(false));
    }
}