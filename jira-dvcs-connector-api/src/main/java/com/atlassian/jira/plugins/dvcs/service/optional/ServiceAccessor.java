package com.atlassian.jira.plugins.dvcs.service.optional;

import java.util.Optional;

/**
 * An interface for service accessors that yield optional services if they are available
 */
public interface ServiceAccessor<T> {
    /**
     * Get the service, if it is available
     *
     * @return The service, if it is available
     */
    Optional<T> get();

    /**
     * Check if the service is available on this accessor
     *
     * @return If the service is available on this accessor
     */
    default boolean isAvailable() {
        return get().isPresent();
    }

}
