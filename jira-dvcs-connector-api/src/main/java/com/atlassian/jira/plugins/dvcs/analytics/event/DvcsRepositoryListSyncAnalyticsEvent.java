package com.atlassian.jira.plugins.dvcs.analytics.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.model.Organization;

import javax.annotation.Nonnull;

@EventName("jira.dvcsconnector.org.repos.sync")
public class DvcsRepositoryListSyncAnalyticsEvent {
    private final int organizationId;
    private final AuthType authType;
    private final String dvcsType;
    private final int numNewRepos;
    private final int totalNumRepos;

    public DvcsRepositoryListSyncAnalyticsEvent(@Nonnull final Organization org, int numNewRepos, int totalNumRepos) {
        this.organizationId = org.getId();
        this.authType = AuthType.fromCredential(org.getCredential());
        this.dvcsType = org.getDvcsType();
        this.numNewRepos = numNewRepos;
        this.totalNumRepos = totalNumRepos;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public String getDvcsType() {
        return dvcsType;
    }

    public int getNumNewRepos() {
        return numNewRepos;
    }

    public int getTotalNumRepos() {
        return totalNumRepos;
    }
}
