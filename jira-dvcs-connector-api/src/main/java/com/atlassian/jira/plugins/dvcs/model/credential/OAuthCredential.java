package com.atlassian.jira.plugins.dvcs.model.credential;

import javax.annotation.Nonnull;

/**
 * An OAuth credential that provides at least a key and secret.
 * <p>
 * Implementations may provide additional fields specific the version/level of OAuth authentication.
 */
public interface OAuthCredential extends Credential {

    @Nonnull
    String getKey();

    @Nonnull
    String getSecret();

}
