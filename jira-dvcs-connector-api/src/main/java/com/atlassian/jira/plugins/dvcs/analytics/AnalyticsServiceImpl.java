package com.atlassian.jira.plugins.dvcs.analytics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsAddUserAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddEndedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsConfigAddStartedAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsInviteGroupChanged;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsRepositoryListSyncAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsSyncEndAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsSyncStartAnalyticsEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsType;
import com.atlassian.jira.plugins.dvcs.analytics.event.DvcsUserBitbucketInviteSent;
import com.atlassian.jira.plugins.dvcs.analytics.event.FailureReason;
import com.atlassian.jira.plugins.dvcs.analytics.event.Source;
import com.atlassian.jira.plugins.dvcs.analytics.event.connect.ConnectOrganizationAddedEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.connect.ConnectOrganizationMigratedEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.connect.ConnectOrganizationRemovedEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.connect.ConnectOrganizationUpdatedEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.lifecycle.OrganizationApprovalFailedEvent;
import com.atlassian.jira.plugins.dvcs.analytics.event.lifecycle.OrganizationApprovedEvent;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

@Named("analyticsService")
@ParametersAreNonnullByDefault
public class AnalyticsServiceImpl implements AnalyticsService {
    private EventPublisher eventPublisher;

    @Inject
    public AnalyticsServiceImpl(final EventPublisher eventPublisher) {
        checkNotNull(eventPublisher);
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void publishInviteGroupChange(final int inviteGroupsEnabled) {
        DvcsInviteGroupChanged dvcsInviteGroupChanged = new DvcsInviteGroupChanged(inviteGroupsEnabled);
        eventPublisher.publish(dvcsInviteGroupChanged);
    }

    @Override
    public void publishUserCreatedThatHasInvite() {
        DvcsAddUserAnalyticsEvent dvcsAddUserAnalyticsEvent = new DvcsAddUserAnalyticsEvent();
        eventPublisher.publish(dvcsAddUserAnalyticsEvent);
    }

    @Override
    public void publishInviteSent() {
        DvcsUserBitbucketInviteSent dvcsUserInviteEmailSent = new DvcsUserBitbucketInviteSent();
        eventPublisher.publish(dvcsUserInviteEmailSent);
    }

    @Override
    public void publishRepositorySyncStart(final int syncId, @Nullable final Repository repo,
                                           final Set<SynchronizationFlag> flags) {
        eventPublisher.publish(new DvcsSyncStartAnalyticsEvent(syncId, repo, checkNotNull(flags)));
    }

    @Override
    public void publishRepositorySyncEnd(final int syncId, final Date finishedOn, final long timeInMillis) {
        eventPublisher.publish(new DvcsSyncEndAnalyticsEvent(syncId, finishedOn, timeInMillis));
    }

    @Override
    public void publishRepositoryListUpdated(final Organization org, final int numberNewRepos,
                                             final int totalNumberRepos) {
        eventPublisher.publish(new DvcsRepositoryListSyncAnalyticsEvent(org, numberNewRepos, totalNumberRepos));
    }

    @Override
    public void publishOrganisationAddStarted(final Source source, final DvcsType dvcsType) {
        eventPublisher.publish(new DvcsConfigAddStartedAnalyticsEvent(source, dvcsType));
    }

    @Override
    public void publishOrganisationAddSucceeded(final Source source, final Organization org) {
        eventPublisher.publish(DvcsConfigAddEndedAnalyticsEvent.createSucceeded(source, org));
    }

    @Override
    public void publishOrganisationAddFailed(final Source source, final DvcsType dvcsType, final FailureReason reason) {
        eventPublisher.publish(DvcsConfigAddEndedAnalyticsEvent.createFailed(source, dvcsType, reason));
    }

    @Override
    public void publishConnectOrganisationAdded(final Organization org) {
        eventPublisher.publish(new ConnectOrganizationAddedEvent(checkNotNull(org)));
        publishOrganisationAddSucceeded(Source.DEVTOOLS, org);
    }

    @Override
    public void publishConnectOrganisationMigrated(final Organization org) {
        eventPublisher.publish(new ConnectOrganizationMigratedEvent(checkNotNull(org)));
    }

    @Override
    public void publishConnectOrganisationUpdated(final Organization org) {
        eventPublisher.publish(new ConnectOrganizationUpdatedEvent(checkNotNull(org)));
    }

    @Override
    public void publishConnectOrganisationRemoved(final Organization org) {
        eventPublisher.publish(new ConnectOrganizationRemovedEvent(checkNotNull(org)));
    }

    @Override
    public void publishOrganizationApproved(final Organization organization,
                                            final OrganizationApprovalLocation approvalLocation) {
        eventPublisher.publish(new OrganizationApprovedEvent(organization, approvalLocation));
    }

    @Override
    public void publishOrganizationApprovalFailed(final int organizationId,
                                                  final OrganizationApprovalLocation approvalLocation,
                                                  final OrganizationApprovalFailedReason reason) {
        eventPublisher.publish(new OrganizationApprovalFailedEvent(organizationId, approvalLocation, reason));
    }
}
